$(function() {
    /* 
       Custom js file for assan
       */
    //preloader
    $(window).preloader({
        delay: 200,
    });
    $(".nav-sticky-top").sticky({ topSpacing: 0 });
    //shrink header
    $(document).on("scroll", function() {
        if ($(document).scrollTop() > 150) {
            $(".nav-sticky").addClass("nav-stick-top");
        } else {
            $(".nav-sticky").removeClass("nav-stick-top");
        }
    });
    /****************
       search inline
       */
    $(".search-open").on("click", function() {
        {
            $(".search-inline").addClass("search-visible");
        }
    });
    $(".search-close").on("click", function() {
        $(".search-inline").removeClass("search-visible");
    });
    //back to top
    if ($("#back-to-top").length) {
        var scrollTrigger = 100, // px
            backToTop = function() {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $("#back-to-top").addClass("show");
                } else {
                    $("#back-to-top").removeClass("show");
                }
            };
        backToTop();
        $(window).on("scroll", function() {
            backToTop();
        });
        $("#back-to-top").on("click", function(e) {
            e.preventDefault();
            $("html,body").animate({
                    scrollTop: 0,
                },
                700
            );
        });
    }
    /*****maginific popup **/
    $(".popup-container").each(function() {
        $(this).magnificPopup({
            delegate: "a",
            type: "image",
            mainClass: "mfp-with-zoom",
            gallery: {
                enabled: true,
            },
            zoom: {
                enabled: true,
                duration: 300,
                easing: "ease-in-out",
                opener: function(openerElement) {
                    return openerElement.is("img") ?
                        openerElement :
                        openerElement.find("img");
                },
            },
        });
    });
    /**popup carousel**/
    $(".lightbox-carousel").each(function() {
        // $(this).magnificPopup({
        //   delegate: "a",
        //   type: "image",
        //   mainClass: "mfp-with-zoom",
        //   gallery: {
        //     enabled: true,
        //   },
        //   zoom: {
        //     enabled: true,
        //     duration: 300,
        //     easing: "ease-in-out",
        //     opener: function (openerElement) {
        //       return openerElement.is("img")
        //         ? openerElement
        //         : openerElement.find("img");
        //     },
        //   },
        // });
    });
    /**youtube video popup**/
    $(".modal-video").magnificPopup({
        type: "iframe",
    });
    /**form popup popup**/
    $(".popup-content").magnificPopup({
        type: "inline",
        mainClass: "mfp-with-zoom",
        preloader: true,
    });
    /**on load modal**/
    setTimeout(function() {
        if ($("#onloadModal").length) {
            $.magnificPopup.open({
                items: {
                    src: "#onloadModal",
                },
                type: "inline",
            });
        }
    }, 1000);
    /**sticky sidebar**/
    jQuery(".sticky-content, .sticky-sidebar").theiaStickySidebar({
        // Settings
        additionalMarginTop: 30,
    });
    /**Carousel images**/
    $(".carousel-image").owlCarousel({
        loop: true,
        margin: 15,
        nav: true,
        navText: [
            "<i class='ti-arrow-left'></i>",
            "<i class='ti-arrow-right'></i>",
        ],
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,
            },
        },
    });
    /**Carousel popup**/
    if ($(window).width() < 854) {
        $(".carousel-popup").owlCarousel({
            loop: true,
            margin: 15,
            nav: false,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: false,

            navText: [
                "<i class='ti-arrow-left'></i>",
                "<i class='ti-arrow-right'></i>",
            ],
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 2,
                },
                1000: {
                    items: 4,
                },
            },
        });
    } else {
        $(".carousel-popup").owlCarousel({
            loop: true,
            margin: 15,
            nav: true,
            autoplay: true,
            autoplayTimeout: 2000,
            autoplayHoverPause: false,

            navText: [
                "<i class='ti-arrow-left'></i>",
                "<i class='ti-arrow-right'></i>",
            ],
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 2,
                },
                1000: {
                    items: 4,
                },
            },
        });
    }

    /**Carousel project**/
    $(".carousel-project").owlCarousel({
        loop: true,
        margin: 15,
        nav: false,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 2,
            },
        },
    });
    /**Carousel project**/
    $(".carousel-project2").owlCarousel({
        loop: true,
        margin: 15,
        nav: false,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 2,
            },
        },
    });
    /**Carousel project**/
    $(".carousel-product").owlCarousel({
        loop: true,
        margin: 15,
        nav: false,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 4,
            },
        },
    });
    /**Carousel feature**/
    $(".carousel-feature").owlCarousel({
        loop: true,
        margin: 15,
        nav: false,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 4,
            },
        },
    });
    /**Carousel Clients**/
    $(".carousel-client").owlCarousel({
        loop: true,
        margin: 15,
        nav: false,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: false,
        responsive: {
            0: {
                items: 2,
                rows: 2,
            },
            600: {
                items: 3,
                rows: 2,
            },
            1000: {
                items: 5,
                rows: 2,
            },
        },
    });
    /**Carousel Clients**/
    $(".carousel-testimonial").owlCarousel({
        loop: true,
        margin: 15,
        nav: true,
        autoHeight: true,
        dots: false,
        navText: [
            "<i class='ti-angle-left'></i>",
            "<i class='fas fa-arrow-right'></i>",
        ],
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,
            },
        },
    });
    $(".carousel-category").owlCarousel({
        loop: true,
        margin: 2,
        nav: true,
        autoHeight: true,
        navText: [
            "<i class='ti-angle-left'></i>",
            "<i class='fas fa-arrow-right'></i>",
        ],
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 4,
            },
        },
    });
    $(".carousel-testimonial-dark").owlCarousel({
        loop: true,
        margin: 15,
        nav: true,
        autoHeight: true,
        navText: [
            "<i class='ti-angle-left'></i>",
            "<i class='fas fa-arrow-right'></i>",
        ],
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,
            },
        },
    });
    $(".carousel-testimonial-grid").owlCarousel({
        loop: true,
        margin: 15,
        nav: true,
        autoHeight: true,
        navText: [
            "<i class='ti-angle-left'></i>",
            "<i class='fas fa-arrow-right'></i>",
        ],
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 2,
            },
        },
    });

    var owl = $('#welcome-carousel');
    owl.owlCarousel({
        loop: true,
        nav: false,
        margin: 0,
        autoHeight: true,
        dots: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: false,
        mouseDrag: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            960: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
    });

    var owls = $('#home-carousel');
    owls.owlCarousel({
        loop: true,
        nav: false,
        margin: 0,
        autoHeight: true,
        dots: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: false,
        mouseDrag: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            960: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
    });

    function rateLimit(func, time) {
        var callback = func,
            waiting = false,
            context = this;
        var rtn = function() {
            if (waiting) return;
            waiting = true;
            var args = arguments;
            setTimeout(function() {
                waiting = false;
                callback.apply(context, args);
            }, time);
        };
        return rtn;
    }

    function onWheel(e) {
        if (e.deltaY > 0) {
            owl.trigger('next.owl');
        } else {
            owl.trigger('prev.owl');
        }
        e.preventDefault();
    }

    var debouncedOnWheel = rateLimit(onWheel, 1000);
    window.addEventListener("wheel", debouncedOnWheel);

    // var owl = $('.owl-main-slide');
    // owl.owlCarousel({
    //     loop:true,
    //     nav:false,
    //     margin:0,
    //     autoHeight: true,
    //     dots: true,
    //     autoplay:true,
    //     autoplayTimeout:5000,
    //     autoplayHoverPause:false,
    //     mouseDrag: true,
    //     animateOut: 'fadeOut',
    //     animateIn: 'fadeIn',
    //     responsive:{
    //         0:{
    //             items:1
    //         },
    //         600:{
    //             items:1
    //         },            
    //         960:{
    //             items:1
    //         },
    //         1200:{
    //             items:1
    //         }
    //     }
    // });
    // owl.on('wheel', '.owl-stage', function (e) {
    //     if (e.deltaY>0) {
    //         owl.trigger('next.owl');
    //     } else {
    //         owl.trigger('prev.owl');
    //     }
    //     e.preventDefault();
    // });

    // var timestamp_mousewheel = 0; //Define it not in a function should
    // owl.on("mousewheel", ".owl-stage", function(a) {
    // var d = new Date();
    // if((d.getTime() - timestamp_mousewheel) > 1000){ //minimum time difference

    //   owl.on('mousewheel', '.owl-stage', function (e) {
    //     timestamp_mousewheel = d.getTime();
    //     console.log(timestamp_mousewheel);
    //     console.log(e.deltaY);

    //     if (e.deltaY > 0) {
    //         owl.trigger('next.owl');
    //     } else {
    //         owl.trigger('prev.owl');
    //     }
    //         e.preventDefault();
    //     });
    //   }
    // });

    wow = new WOW({
        boxClass: "wow",
        animateClass: "animated",
        offset: 0,
        mobile: true,
        live: true,
    });
    wow.init();

    //counter
    $(".counter").waypoint(
        function() {
            $(".counter").countTo();
        }, { offset: "100%" }
    );
    //countdown
    var time = $(".count-down");
    if (time.length) {
        var endDate = new Date(time.data("end-date"));
        time.countdown({
            date: endDate,
            render: function(data) {
                $(this.el).html(
                    '<div class="cd-row"><div><h1>' +
                    this.leadingZeros(data.days, 3) +
                    "</h1><p>days</p></div><div><h1>" +
                    this.leadingZeros(data.hours, 2) +
                    '</h1><p>hrs</p></div></div><div class="cd-row"><div><h1>' +
                    this.leadingZeros(data.min, 2) +
                    "</h1><p>min</p></div><div><h1>" +
                    this.leadingZeros(data.sec, 2) +
                    "</h1><p>sec</p></div></div>"
                );
            },
        });
    }

    //tooltip
    $('[data-toggle="tooltip"]').tooltip();
    //popover
    $('[data-toggle="popover"]').popover();
    //knob circle progress bar
    $(".progress-circle").knob();
    //text illate
    $(".tlt").textillate({
        loop: true,
    });
    //smooth scroll
    smoothScroll.init({
        selector: "[data-scroll]", // Selector for links (must be a class, ID, data attribute, or element tag)
        speed: 1000, // Integer. How fast to complete the scroll in milliseconds
        easing: "easeInOutCubic", // Easing pattern to use
        offset: 0, // Integer. How far to offset the scrolling anchor location in pixels
        callback: function(anchor, toggle) {}, // Function to run after scrolling
    });

    //particle
    $("#particles").particleground({
        dotColor: "#eee",
        lineColor: "#eee",
    });

    //blog masonry
    var $container = $("#blog-masonry");
    $container.imagesLoaded(function() {
        $container.masonry({
            itemSelector: ".post-masonry",
        });
    });

    //product card
    $(".sheetPile").click(function() {
        window.location = $(this).find("a").attr("href");
        return false;
    });

    $(".squarePile").click(function() {
        window.location = $(this).find("a").attr("href");
        return false;
    });

    $(".hydraulicInjection").click(function() {
        window.location = $(this).find("a").attr("href");
        return false;
    });

    $(".dieselHammer").click(function() {
        window.location = $(this).find("a").attr("href");
        return false;
    });

    $(".drillingServices").click(function() {
        window.location = $(this).find("a").attr("href");
        return false;
    });

    $(".lateralLoading").click(function() {
        window.location = $(this).find("a").attr("href");
        return false;
    });

    $(".pileDriving").click(function() {
        window.location = $(this).find("a").attr("href");
        return false;
    });

    $(".staticLoading").click(function() {
        window.location = $(this).find("a").attr("href");
        return false;
    });

    $("#coba").DataTable({
        scrollX: false,
    });

    $("#admin").DataTable({
        scrollX: false,
    });

    $(".typed").typed({
        strings: ["Beautifully", "Easily", "Fast"],
        typeSpeed: 50,
        backSpeed: 10,
        backDelay: 2000,
        showCursor: false,
        loop: true,
    });
});

function openSquare(evt, squareName) {
    var i, x, tablinks;
    x = document.getElementsByClassName("square");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < x.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" w3-border-wahana-blue", "");
    }
    document.getElementById(squareName).style.display = "block";
    evt.currentTarget.firstElementChild.className += " w3-border-wahana-blue";
};

function openSheet(evt, squareName) {
    var i, x, tablinks;
    x = document.getElementsByClassName("sheet");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < x.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" w3-border-wahana-blue", "");
    }
    document.getElementById(squareName).style.display = "block";
    evt.currentTarget.firstElementChild.className += " w3-border-wahana-blue";
};