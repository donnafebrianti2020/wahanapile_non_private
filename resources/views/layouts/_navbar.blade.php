<nav class="navbar navbar-expand-lg navbar-light navbar-transparent bg-faded nav-sticky">
    <div class="container"><button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
            aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button> <a class="navbar-brand"
            href="{{ route('landing') }}"> <img class='logo logo-dark'
                src="{{ asset('assets/images/logo/wahana/wahanapile-3.png') }}" alt="Wahana Logo" style="height: 50px;">
            <img class='logo logo-light hidden-md-down' src="{{ asset('assets/images/logo/wahana/wahanapile-3.png') }}"
                alt="Wahana Logo" style="height: 50px;">
        </a>
        <div id="navbarNavDropdown" class="navbar-collapse collapse">
            <ul class="navbar-nav ml-auto">
                @if('dashboard' == Route::currentRouteName())
                    <li class=" nav-item d-none d-lg-block"> <a role="button" class="nav-link navlink-dark btn btn-rounded btn-primary ml-2" style="align-text: center"
                            href="{{ route('login') }}"> LOGOUT </a></li>
                    <li class=" nav-item d-block d-lg-none"> <a class="nav-link btnnav"
                            href="{{ route('login') }}">LOGOUT</a></li>
                @endif
                @if('project-form' == Route::currentRouteName())
                    <li class=" nav-item d-none d-lg-block"> <a role="button" class="nav-link navlink-dark btn btn-rounded btn-info ml-2" style="align-text: center"
                            href="{{ route('dashboard') }}"> BACK </a></li>
                    <li class=" nav-item d-block d-lg-none"> <a class="nav-link btnnav"
                            href="{{ route('dashboard') }}">BACK</a></li>
                    <li class=" nav-item d-none d-lg-block"> <a role="button" class="nav-link navlink-dark btn btn-rounded btn-primary ml-2" style="align-text: center"
                            href="{{ route('login') }}"> LOGOUT </a></li>
                    <li class=" nav-item d-block d-lg-none"> <a class="nav-link btnnav"
                            href="{{ route('login') }}">LOGOUT</a></li>
                @endif
                @if('login' == Route::currentRouteName())
                    <li class=" nav-item d-none d-lg-block"> <a role="button" class="nav-link navlink-dark btn btn-rounded btn-primary ml-2" style="align-text: center"
                            href="{{ route('landing') }}"> BACK TO HOME </a></li>
                    <li class=" nav-item d-block d-lg-none"> <a class="nav-link btnnav"
                            href="{{ route('landing') }}">BACK TO HOME</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>