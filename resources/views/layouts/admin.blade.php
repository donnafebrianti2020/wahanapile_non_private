<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/images/favicon/apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/images/favicon/apple-touch-icon-114x114.png') }}">    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/images/favicon/apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/images/favicon/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/images/favicon/apple-touch-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/images/favicon/apple-touch-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/images/favicon/apple-touch-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/images/favicon/apple-touch-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/images/favicon/apple-touch-icon-152x152.png') }}">
    <link rel="icon" type="image/png') }}" sizes="32x32" href="{{ asset('assets/images/favicon/icon.png') }}">
    <link rel="icon" type="image/png') }}" sizes="16x16" href="{{ asset('assets/images/favicon/icon.png') }}">
    <link rel="manifest" href="{{ asset('assets/images/favicon/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ asset('assets/images/favicon/safari-pinned-tab.svg') }}" color="#000000">
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon/icon.png') }}">
    <meta name="msapplication-TileColor" content="#00aba9">
    <meta name="msapplication-config" content="{{ asset('assets/images/favicon/browserconfig.xml') }}">
    <meta name="theme-color" content="#ffffff">
    <meta name="description"
        content="PT WAHANA CIPTA CONCRETINDO - PRECAST - PRESTRESSED CONCRETE PRODUCTS">
    <meta name="viewport" content="width=device-width, initial-scale=0.9, shrink-to-fit=no">
    <title>PT Wahana Cipta Concretindo</title>
    
    <!-- Default Styles -->
    @include('layouts._styles')
    
    <!-- Other Styles -->
    @stack('other-styles')
</head>

<body id="index">
    <div id="preloader">
        <div id="preloader-inner"></div>
    </div>
    <div class="site-overlay"></div>
    <!-- Default Styles -->

    @include('layouts._navbar')

    @yield('content')

    <!-- @if (!Request::routeIs('landing')) {{-- remove footer when on home-mobile page --}}
    @endif -->
    
    <a href="#" class="back-to-top" id="back-to-top"><i class="ti-angle-up"></i></a>

    <!-- Default Scripts -->
    @include('layouts._scripts')

    <!-- Other Scripts -->
    @stack('other-scripts')

    
</body>

</html>