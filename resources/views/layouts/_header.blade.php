<nav class="navbar navbar-expand-lg navbar-light navbar-transparent bg-faded nav-sticky">
    <!-- <div class="search-inline">
        <form> <input type="text" class="form-control" placeholder="Type and hit enter..."> <button type="submit"><i
                    class="ti-search"></i></button> <a href="javascript:void(0)" class="search-close"><i
                    class="ti-close"></i></a></form>
    </div> -->
    <div class="container"><button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
            aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button> <a class="navbar-brand"
            href="{{ route('landing') }}"> <img class='logo logo-dark'
                src="{{ asset('assets/images/logo/wahana/wahanapile-3.png') }}" alt="Wahana Logo" style="height: 50px;">
            <img class='logo logo-light hidden-md-down' src="{{ asset('assets/images/logo/wahana/wahanapile-3.png') }}"
                alt="Wahana Logo" style="height: 50px;">
        </a>
        <div id="navbarNavDropdown" class="navbar-collapse collapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item {{ Request::segment(1) == '' ? 'active' : '' }}"> <a class="nav-link btnnav"
                        href="{{ route('landing') }}">HOME</a></li>
                <li class="nav-item {{ Request::segment(1) == 'about-us' ? 'active' : '' }}"> <a class="nav-link btnnav"
                        href="{{ route('about-us') }}">TENTANG KAMI</a></li>
                <li class="nav-item {{ Request::segment(1) == 'product' ? 'active' : '' }} {{ Request::segment(1) == 'square-pile' ? 'active' : '' }} {{ Request::segment(1) == 'sheet-pile' ? 'active' : '' }}"> <a class="nav-link btnnav"
                        href="{{ route('product') }}">PRODUCT</a></li>
                <li class=" nav-item {{ Request::segment(1) == 'service' ? 'active' : '' }} {{ Request::segment(1) == 'hydraulic-injection' ? 'active' : '' }} {{ Request::segment(1) == 'diesel-hammer' ? 'active' : '' }} {{ Request::segment(1) == 'drilling-services' ? 'active' : '' }} {{ Request::segment(1) == 'lateral-loading' ? 'active' : '' }} {{ Request::segment(1) == 'pile-driving' ? 'active' : '' }} {{ Request::segment(1) == 'static-loading' ? 'active' : '' }}"> <a class="nav-link btnnav"
                        href="{{ route('service') }}">SERVICE</a></li>
                <li class=" nav-item {{ Request::segment(1) == 'project' ? 'active' : '' }}"> <a class="nav-link btnnav"
                        href="{{ route('project') }}">PROJECT</a></li>
                <li class=" nav-item d-none d-lg-block"> <a role="button" class="nav-link navlink-dark btn btn-rounded btn-primary ml-2" style="align-text: center"
                        href="{{ route('contact-us') }}"> HUBUNGI KAMI </a></li>
                <li class=" nav-item d-block d-lg-none {{ Request::segment(1) == 'contact-us' ? 'active' : '' }}"> <a class="nav-link btnnav"
                        href="{{ route('contact-us') }}">HUBUNGI KAMI</a></li>
            </ul>
        </div>
    </div>
</nav>

<div id="wa">
        <button class="close-sticky close" onclick="document.getElementById('wa').style.display='none'">
                <img class="wow zoomIn animated" src="assets/images/icon/close.svg" draggable="false">
        </button>
        <a href="https://wa.me/62895628095939">
                <img class="wa-sticky wow zoomIn animated"src="assets/images/sticky/sticky-1.png" draggable="false">
        </a>
</div>
