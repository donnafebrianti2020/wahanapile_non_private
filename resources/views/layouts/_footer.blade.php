<div class="footer-bottomAlt d-none d-lg-block">
    <div class="container"><button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
        <div class="row d-flex align-items-center justify-content-start">
            <!-- <a class="navbar-brand" href="{{ route('landing') }}">
                <img class='logo logo-dark' src="{{ asset('assets/images/logo/wahana/wahana2.png') }}" alt="Genesys Logo" style="height: 50px;">
            </a> -->
            <a class="nav-link inactive"><strong> © 2022. PT Wahana Cipta Concretindo </strong> </a>
            <a class="nav-link" href="{{ route('landing') }}">Home</a>
            <a class="nav-link" href="{{ route('about-us') }}">Tentang Kami</a>
            <a class="nav-link" href="{{ route('product') }}">Product</a>
            <a class="nav-link" href="{{ route('service') }}">Service</a>
            <a class="nav-link" href="{{ route('project') }}"> Project </a>
            <a class="nav-link" href="{{ route('contact-us') }}"> Hubungi Kami </a>
            <div class="list-inline footer-social pl-2">
                <a href="https://www.instagram.com/wahanapile/" class="social-icon si-dark si-gray-round si-colored-google-plus" target="_blank" rel="noopener"> <i class="fab fa-instagram"></i> <i class="fab fa-instagram"></i> </a>
                <a href="https://wa.me/62895628095939" class="social-icon si-dark si-gray-round si-colored-whatsapp" target="_blank" rel="noopener"> <i class="fab fa-whatsapp"></i> <i class="fab fa-whatsapp"></i> </a>
            </div>
        </div>
    </div>
</div>
<div class="footer-bottomAlt d-block d-lg-none">
    <div class="container"><button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
        <div class="row d-flex align-items-center justify-content-center">
            <a class="nav-link" style="padding-right:0px !important; padding-left:0px !important;" href="{{ route('about-us') }}">Home</a>
            <a class="nav-link" style="padding-right:0px !important;" href="{{ route('about-us') }}">Tentang Kami</a>
            <a class="nav-link" style="padding-right:0px !important;" href="{{ route('product') }}">Product</a>
            <a class="nav-link" style="padding-right:0px !important;" href="{{ route('service') }}">Service</a>
            <a class="nav-link" style="padding-right:0px !important;" href="{{ route('project') }}"> Project </a>
        </div>
        <div class="row d-flex align-items-center justify-content-center">
            <a class="nav-link inactive"><strong> © 2022. PT Wahana Cipta Concretindo </strong> </a>
        </div>
        <div class="row d-flex align-items-center justify-content-center pt-2">
            {{-- <a class="navbar-brand" href="{{ route('landing') }}">
                <img class='logo logo-dark' src="{{ asset('assets/images/logo/wahana/wahana2.png') }}" alt="Logo Wahana Pile" style="height: 50px;">
            </a> --}}
            <a href="https://www.instagram.com/wahanapile/" class="social-icon si-dark si-gray-round si-colored-google-plus" target="_blank" rel="noopener"> <i class="fab fa-instagram"></i> <i class="fab fa-instagram"></i> </a>
            <a href="https://wa.me/62895628095939" class="social-icon si-dark si-gray-round si-colored-whatsapp" target="_blank" rel="noopener"> <i class="fab fa-whatsapp"></i> <i class="fab fa-whatsapp"></i> </a>
        </div>
    </div>
</div>
<!-- <div class="center" style="background-color: black;">
    <p style="color: white; font-size: 12px; padding-top: 10px; padding-bottom: 10px;">
        © 2022. PT Wahana Cipta Concretindo
    </p>
</div> -->