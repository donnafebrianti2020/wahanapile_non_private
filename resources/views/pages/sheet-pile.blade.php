@extends('layouts.app')

@push('other-styles')
@endpush

@section('content')
@include('components._sheet-pile-banner')
@include('components._sheet-pile-tab')
@include('components._pre-footer')

@endsection

@push('other-scripts')
@endpush