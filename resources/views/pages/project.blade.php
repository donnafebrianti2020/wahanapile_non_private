@extends('layouts.app')

@push('other-styles')
@endpush

@section('content')
@include('components._project-banner')
@include('components._project-table')
@include('components._get-in-touch')
@include('components._pre-footer')

@endsection

@push('other-scripts')
@endpush