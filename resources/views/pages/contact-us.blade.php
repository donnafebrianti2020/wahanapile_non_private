@extends('layouts.app')

@push('other-styles')
@endpush

@section('content')
@include('components._contact-form')
@include('components._maps')
@include('components._pre-footer')

@endsection

@push('other-scripts')
@endpush