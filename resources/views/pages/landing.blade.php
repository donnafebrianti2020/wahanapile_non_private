@extends('layouts.app')

@push('other-styles')
@endpush

@section('content')
@include('components._home-carousel')
@include('components._homeprofile')
@include('components._product-service')
@include('components._proyek')
@include('components._clients')
@include('components._get-in-touch')
@include('components._pre-footer')

@endsection

@push('other-scripts')
@endpush