@extends('layouts.app')

@push('other-styles')
@endpush

@section('content')
@include('components._about-us')
@include('components._advantage')
@include('components._get-in-touch')
@include('components._pre-footer')
@endsection

@push('other-scripts')
@endpush