@extends('layouts.app')

@push('other-styles')
@endpush

@section('content')
@include('components._square-pile-banner')
@include('components._square-pile-tab')
@include('components._pre-footer')

@endsection

@push('other-scripts')
@endpush