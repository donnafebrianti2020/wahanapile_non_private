@extends('layouts.app')

@push('other-styles')
@endpush

@section('content')
@include('components._product-category')
@include('components._advantage')
@include('components._pre-footer')

@endsection

@push('other-scripts')
@endpush