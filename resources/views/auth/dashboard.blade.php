@extends('layouts.admin')

@push('other-styles')
@endpush

@section('content')
@include('components._admin-project-table')
@endsection

@push('other-scripts')
@endpush