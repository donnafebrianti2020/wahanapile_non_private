<section id="about-us" class="bg-custom-image mt200 mb50" style='background-image: url("assets/images/about-us/about-us-bg.jpg");'>
    <div class="pt20 pb40 d-block d-lg-none" style="background-color: rgba(255, 255, 255, 0.9)">
        <div class="container">
            <div class="row justify-content-center">
                <h2 class="text-bold text-22 text-center wow zoomIn animated">
                    PT WAHANA CIPTA CONCRETINDO
                </h2>
            </div>
            <div class="row justify-content-center">
                <h3 class="text-blue text-16 half-line text-center wow fadeInUp animated">
                    PRECAST - PRESTRESSED CONCRETE PRODUCTS
                </h3>
            </div>
            <div class="row justify-content-center">
                <img class="main-image-sm wow fadeInUp animated" src="assets/images/about-us/about-us-main-image-4.jpg" draggable="false">
            </div>
            <div class="row justify-content-center">
                <p class="text-14 main-content-sm text-bold wow fadeInUp animated">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </p>
            </div>
            <div class="row justify-content-center">
                <form method="get" action="assets/document/wahana-pile-company-profile.pdf">
                    <button type="submit" class="btn btn-primary text-white wow zoomIn animated">
                        Download Company Profile
                    </button>
                </form>
            </div>
        </div>
    </div>
    <div class="pt100 pb40 d-none d-lg-block" style="background-color: rgba(255, 255, 255, 0.9)">
        <div class="container">
            <div class="row justify-content-center">
                <h2 class="text-bold text-xlarge wow zoomIn animated">
                    PT WAHANA CIPTA CONCRETINDO
                </h2>
            </div>
            <div class="row justify-content-center">
                <h3 class="text-blue text-medium half-line wow fadeInUp animated">
                    PRECAST - PRESTRESSED CONCRETE PRODUCTS
                </h3>
            </div>
            <div class="row justify-content-center">
                <img class="main-image wow fadeInUp animated" src="assets/images/about-us/about-us-main-image-4.jpg" draggable="false">
            </div>
            <div class="row justify-content-center">
                <p class="text-14 main-content text-bold wow fadeInUp animated">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </p>
            </div>
            <div class="row justify-content-center">
                <form method="get" action="assets/document/wahana-pile-company-profile.pdf">
                    <button type="submit" class="btn btn-primary text-white wow zoomIn animated">
                        Download Company Profile
                    </button>
                </form>
            </div>
        </div>
    </div>
    <div class="pt40 pb40 gradient-bg">
        <div class="container">
            <div class="row justify-content-center pt-3">
                <h2 class="text-bold text-white text-xlarge text-center wow fadeInUp animated">
                    LATAR BELAKANG PERUSAHAAN
                </h2>
            </div>
            <div class="row justify-content-center pt-5 pb-5">
                <div class="col-5 d-none d-md-block va-middle">
                    <img class="content-image" src="assets/images/about-us/latar-belakang-3.jpg" draggable="false">
                </div>
                <div class="col-12 d-block d-md-none pr-5">
                    <div class="timeline">
                        <div class="container right wow zoomInRight animated">
                            <div class="content">
                                <h3 class="text-medium text-bold mt-1"> 2016 </h3>
                                <h3 class="text-sm">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </h3>
                            </div>
                        </div>
                        <div class="container right wow zoomInRight animated">
                            <div class="content">
                                <h3 class="text-medium text-bold mt-1"> 2010 </h3>
                                <h3 class="text-sm">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </h3>
                            </div>
                        </div>
                        <div class="container right wow zoomInRight animated">
                            <div class="content">
                                <h3 class="text-medium text-bold mt-1"> 2003 </h3>
                                <h3 class="text-sm">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </h3>
                            </div>
                        </div>
                        <div class="container right wow zoomInRight animated">
                            <div class="content">
                                <h3 class="text-medium text-bold mt-1"> 1990 </h3>
                                <h3 class="text-sm">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-7 d-none d-md-block">
                    <div class="timeline">
                        <div class="container right wow zoomInRight animated">
                            <div class="content">
                                <h3 class="text-medium text-bold mt-1"> 2016 </h3>
                                <h3 class="text-small">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </h3>
                            </div>
                        </div>
                        <div class="container right wow zoomInRight animated">
                            <div class="content">
                                <h3 class="text-medium text-bold mt-1"> 2010 </h3>
                                <h3 class="text-small">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </h3>
                            </div>
                        </div>
                        <div class="container right wow zoomInRight animated">
                            <div class="content">
                                <h3 class="text-medium text-bold mt-1"> 2003 </h3>
                                <h3 class="text-small">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </h3>
                            </div>
                        </div>
                        <div class="container right wow zoomInRight animated">
                            <div class="content">
                                <h3 class="text-medium text-bold mt-1"> 1990 </h3>
                                <h3 class="text-small">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pt80 pb40" style="background-color: rgba(255, 255, 255, 0.9)">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-6 pr-4">
                    <div class="container">
                        <div class="row justify-content-center pb-2">
                            <img class="content-image wow zoomIn animated" src="assets/images/vision-mission/vision.jpg" draggable="false">
                        </div>
                        <div class="row justify-content-center pt-4">
                            <h2 class="text-bold text-xlarge text-blue wow fadeInUp animated">
                                VISI
                            </h2>
                        </div>
                        <div class="row justify-content-center pt-2 pl-3">
                            <p class="text-14 text-justify wow fadeInUp animated">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 pl-md-5 pr-4 pt-4">
                    <div class="container">
                        <div class="row justify-content-center pb-2">
                            <img class="content-image wow zoomIn animated" src="assets/images/vision-mission/mission.jpg" draggable="false">
                        </div>
                        <div class="row justify-content-center pt-4">
                            <h2 class="text-bold text-xlarge text-blue wow fadeInUp animated">
                                MISI
                            </h2>
                        </div>
                        <div class="row justify-content-center pt-2 pl-3">
                            <p class="text-14 text-justify wow fadeInUp animated">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pt40 pb40 gradient-bg">
        <div class="container d-block d-lg-none">
            <div class="col-12">
                <div class="row">
                    <h2 class="text-bold dark-content-sm text-xlarge pb-4 wow zoomInLeft animated">
                        NILAI PERUSAHAAN
                    </h2>
                </div>
                <div class="row">
                    <p class="text-14 dark-content-sm wow zoomInLeft animated">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                    </p>
                    <ul class="text-14 dark-content-left pl-4">
                        <li class="pt-3 wow zoomIn animated">
                            Lorem Ipsum is simply dummy text
                        </li>
                        <li class="pt-1 wow zoomIn animated">
                            Printing and typesetting industry
                        </li>
                        <li class="pt-1 wow zoomIn animated">
                            Lorem Ipsum has been the industry's standard dummy
                        </li>
                        <li class="pt-1 wow zoomIn animated">
                            When an unknown printer took a galley of type
                        </li>
                        <li class="pt-1 wow zoomIn animated">
                            But also the leap into electronic typesetting unchanged
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container d-none d-lg-block">
            <div class="row justify-content-center pt-5 pb-5">
                <div class="col-7">
                    <div class="row">
                        <h2 class="text-bold dark-content text-xlarge pb-4 wow zoomInLeft animated">
                            NILAI PERUSAHAAN
                        </h2>
                    </div>
                    <div class="row">
                        <p class="text-14 dark-content wow zoomInLeft animated">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                        </p>
                        <ul class="text-medium dark-content pl-5">
                            <li class="pt-3 wow zoomIn animated">
                                Lorem Ipsum is simply dummy text
                            </li>
                            <li class="pt-3 wow zoomIn animated">
                                Printing and typesetting industry
                            </li>
                            <li class="pt-3 wow zoomIn animated">
                                Lorem Ipsum has been the industry's standard dummy
                            </li>
                            <li class="pt-3 wow zoomIn animated">
                                when an unknown printer took a galley of type
                            </li>
                            <li class="pt-3 wow zoomIn animated">
                                But also the leap into electronic typesetting unchanged
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-5">
                    <img class="content-image wow zoomIn animated" src="assets/images/bg/bg-get-in-touch.jpg" draggable="false">
                </div>
            </div>
        </div>
    </div>
</section>