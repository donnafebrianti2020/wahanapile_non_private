<section id="product-service" class="bg-white-900 pt50 d-block d-lg-none">
    <div class="container">
        <div class="title-heading1 mb40 wow fadeInUp animated">
            <h3>PRODUCT & SERVICES</h3>
        </div>
    </div>
    <div class="pb40" style="background-color: rgba(255, 255, 255, 0.9)">
        <div class="container pt-2">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col-12">
                            <div class="product-card hydraulicInjection wow fadeInUp animated">
                                <div class="row">
                                    <div class="container justify-content-center">
                                        <div class="row pt-3 justify-content-center">
                                            <h2 class="text-bold text-xlarge text-blue text-center">
                                                Square Pile
                                            </h2>
                                        </div>
                                        <div class="row justify-content-center">
                                            <p class="text-14 text-center">
                                                Up to 400 meters production capacity daily
                                            </p>
                                        </div>
                                        <div class="row pb-4 justify-content-center">
                                            <a href="{{ route('square-pile') }}" class="btn btn-primary btn-rounded stretched-link">Lihat produk</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pt-3">
                            <div class="product-card hydraulicInjection wow fadeInUp animated">
                                <div class="row">
                                    <div class="container justify-content-center">
                                        <div class="row pt-3 justify-content-center">
                                            <h2 class="text-bold text-xlarge text-blue text-center">
                                                Sheet Pile
                                            </h2>
                                        </div>
                                        <div class="row justify-content-center">
                                            <p class="text-14 text-center">
                                                Over 1000 mtrs production capacity daily
                                            </p>
                                        </div>
                                        <div class="row pb-4 justify-content-center">
                                            <a href="{{ route('sheet-pile') }}" class="btn btn-primary btn-rounded stretched-link">Lihat produk</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pt-3">
                            <div class="product-card hydraulicInjection wow fadeInUp animated">
                                <div class="row">
                                    <div class="container justify-content-center">
                                        <div class="row pt-3 justify-content-center">
                                            <h2 class="text-bold text-xlarge text-blue text-center">
                                                Hydraulic Injection Piling
                                            </h2>
                                        </div>
                                        <div class="row justify-content-center">
                                            <p class="text-14 text-center">
                                                Silent pile driving process
                                            </p>
                                        </div>
                                        <div class="row pb-4 justify-content-center">
                                            <a href="{{ route('hydraulic-injection') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pt-3">
                            <div class="product-card dieselHammer wow fadeInUp animated">
                                <div class="row">
                                    <div class="container justify-content-center">
                                        <div class="row pt-3 justify-content-center">
                                            <h2 class="text-bold text-xlarge text-blue text-center">
                                                Diesel Hammer Piling
                                            </h2>
                                        </div>
                                        <div class="row justify-content-center">
                                            <p class="text-14 text-center">
                                                Versatile & durable hammer piling rigs
                                            </p>
                                        </div>
                                        <div class="row pb-4 justify-content-center">
                                            <a href="{{ route('diesel-hammer') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 pt-3 pt-md-0">
                    <div class="row">
                        <div class="col-12">
                            <div class="product-card lateralLoading wow fadeInUp animated">
                                <div class="row">
                                    <div class="container justify-content-center">
                                        <div class="row pt-3 justify-content-center">
                                            <h2 class="text-bold text-xlarge text-blue text-center">
                                                Lateral Loading Test
                                            </h2>
                                        </div>
                                        <div class="row justify-content-center">
                                            <p class="text-14 text-center">
                                                Measure the integrity of the piles
                                            </p>
                                        </div>
                                        <div class="row pb-4 justify-content-center">
                                            <a href="{{ route('lateral-loading') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pt-3">
                            <div class="product-card pileDriving wow fadeInUp animated">
                                <div class="row">
                                    <div class="container justify-content-center">
                                        <div class="row pt-3 justify-content-center">
                                            <h2 class="text-bold text-xlarge text-blue text-center">
                                                Pile Driving Analysis Test
                                            </h2>
                                        </div>
                                        <div class="row justify-content-center">
                                            <p class="text-14 text-center">
                                                Accurately measure the pile bearing capacities
                                            </p>
                                        </div>
                                        <div class="row pb-4 justify-content-center">
                                            <a href="{{ route('pile-driving') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pt-3">
                            <div class="product-card staticLoading wow fadeInUp animated">
                                <div class="row">
                                    <div class="container justify-content-center">
                                        <div class="row pt-3 justify-content-center">
                                            <h2 class="text-bold text-xlarge text-blue text-center">
                                                Static Loading Test
                                            </h2>
                                        </div>
                                        <div class="row justify-content-center">
                                            <p class="text-14 text-center">
                                                Discover the bearing capacity of piles
                                            </p>
                                        </div>
                                        <div class="row pb-4 justify-content-center">
                                            <a href="{{ route('static-loading') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pt-3">
                            <div class="product-card drillingServices wow fadeInUp animated">
                                <div class="row">
                                    <div class="container justify-content-center">
                                        <div class="row pt-3 justify-content-center">
                                            <h2 class="text-bold text-xlarge text-blue text-center">
                                                Drilling Services
                                            </h2>
                                        </div>
                                        <div class="row justify-content-center">
                                            <p class="text-14 text-center">
                                                Break through hard soil profiles
                                            </p>
                                        </div>
                                        <div class="row pb-4 justify-content-center">
                                            <a href="{{ route('drilling-services') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="product-service" class="bg-white-900 pt80 pb10 d-none d-lg-block">
    <div class="container">
        <div class="title-heading1 mb40 wow fadeInUp animated">
            <h3>PRODUCT & SERVICES</h3>
        </div>
    </div>
    <div class="pb40" style="background-color: rgba(255, 255, 255, 0.9)">
        <div class="container pt-2">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col-12">
                            <div class="product-card squarePile wow fadeInUp animated">
                                <div class="row justify-content-center pb-2">
                                    <img class="product-category-short" src="assets/images/product/square-pile/square-pile-small.jpg" draggable="false">
                                </div>
                                <div class="row justify-content-center pt-3">
                                    <h2 class="text-bold text-xlarge text-blue">
                                        Square Pile
                                    </h2>
                                </div>
                                <div class="row justify-content-center pt-1 ml30 mr30">
                                    <p class="text-14 text-center">
                                        Up to 4000 meters of total square pile <br>production capacity daily
                                    </p>
                                </div>
                                <div class="row justify-content-center pb-4">
                                    <a href="{{ route('square-pile') }}" class="btn btn-primary btn-rounded stretched-link">LIHAT PRODUK</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pt-4">
                            <div class="product-card sheetPile wow fadeInUp animated">
                                <div class="row justify-content-center pb-2">
                                    <img class="product-category-short" src="assets/images/product/sheet-pile/sheet-pile-small.jpg" draggable="false">
                                </div>
                                <div class="row justify-content-center pt-3">
                                    <h2 class="text-bold text-xlarge text-blue">
                                        Sheet Pile
                                    </h2>
                                </div>
                                <div class="row justify-content-center pt-1 ml30 mr30">
                                    <p class="text-14 text-center">
                                        Over 1000 mtrs of total sheet pile and concrete wall <br>production capacity daily
                                    </p>
                                </div>
                                <div class="row justify-content-center pb-4">
                                    <a href="{{ route('sheet-pile') }}" class="btn btn-primary btn-rounded stretched-link">LIHAT PRODUK</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col-12">
                            <div class="product-card hydraulicInjection wow fadeInUp animated">
                                <div class="row">
                                    <div class="col-md-4 justify-content-center">
                                        <img class="service-icon" src="assets/images/service/hydraulic.jpeg">
                                    </div>
                                    <div class="col-md-7 ml-4">
                                        <div class="row pt-3">
                                            <h2 class="text-bold text-xlarge text-blue">
                                                Hydraulic Injection Piling
                                            </h2>
                                        </div>
                                        <div class="row ml30">
                                            <p class="text-14 text-left">
                                                Silent pile driving process
                                            </p>
                                        </div>
                                        <div class="row pb-4">
                                            <a href="{{ route('hydraulic-injection') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pt-3">
                            <div class="product-card dieselHammer wow fadeInUp animated">
                                <div class="row">
                                    <div class="col-md-4 justify-content-center">
                                        <img class="service-icon" src="assets/images/service/diesel.jpeg">
                                    </div>
                                    <div class="col-md-7 ml-4">
                                        <div class="row pt-3">
                                            <h2 class="text-bold text-xlarge text-blue">
                                                Diesel Hammer Piling
                                            </h2>
                                        </div>
                                        <div class="row ml30">
                                            <p class="text-14 text-left">
                                                Versatile & durable hammer piling rigs
                                            </p>
                                        </div>
                                        <div class="row pb-4">
                                            <a href="{{ route('diesel-hammer') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pt-3">
                            <div class="product-card lateralLoading wow fadeInUp animated">
                                <div class="row">
                                    <div class="col-md-4 justify-content-center">
                                        <img class="service-icon" src="assets/images/service/lateral.jpeg">
                                    </div>
                                    <div class="col-md-7 ml-4">
                                        <div class="row pt-3">
                                            <h2 class="text-bold text-xlarge text-blue">
                                                Lateral Loading Test
                                            </h2>
                                        </div>
                                        <div class="row ml30">
                                            <p class="text-14 text-left">
                                                Measure the integrity of the piles
                                            </p>
                                        </div>
                                        <div class="row pb-4">
                                            <a href="{{ route('lateral-loading') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pt-3">
                            <div class="product-card pileDriving wow fadeInUp animated">
                                <div class="row">
                                    <div class="col-md-4 justify-content-center">
                                        <img class="service-icon" src="assets/images/service/piledriving.jpeg">
                                    </div>
                                    <div class="col-md-7 ml-4">
                                        <div class="row pt-3">
                                            <h2 class="text-bold text-xlarge text-blue">
                                                Pile Driving Analysis Test
                                            </h2>
                                        </div>
                                        <div class="row ml30">
                                            <p class="text-14 text-left">
                                                Accurately measure the pile bearing capacities
                                            </p>
                                        </div>
                                        <div class="row pb-4">
                                            <a href="{{ route('pile-driving') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pt-3">
                            <div class="product-card staticLoading wow fadeInUp animated">
                                <div class="row">
                                    <div class="col-md-4 justify-content-center">
                                        <img class="service-icon" src="assets/images/service/static.jpeg">
                                    </div>
                                    <div class="col-md-7 ml-4">
                                        <div class="row pt-3">
                                            <h2 class="text-bold text-xlarge text-blue">
                                                Static Loading Test
                                            </h2>
                                        </div>
                                        <div class="row ml30">
                                            <p class="text-14 text-left">
                                                Discover the bearing capacity of piles
                                            </p>
                                        </div>
                                        <div class="row pb-4">
                                            <a href="{{ route('static-loading') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- <section id="product-category" class="bg-white-900 pt100 pb20">
    <div class="pb40" style="background-color: rgba(255, 255, 255, 0.9)">
        <div class="container pt-2">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col-12">
                            <div class="product-card hydraulicInjection">
                                <div class="row">
                                    <div class="col-md-4 justify-content-center">
                                        <img class="service-icon" src="assets/images/service/hydraulic.jpg">
                                    </div>
                                    <div class="col-md-7 ml-4">
                                        <div class="row pt-3">
                                            <h2 class="text-bold text-xlarge text-blue">
                                                Hydraulic Injection Piling
                                            </h2>
                                        </div>
                                        <div class="row ml30">
                                            <p class="text-14 text-left">
                                                Silent and environmentally friendly pile driving process
                                            </p>
                                        </div>
                                        <div class="row pb-4">
                                            <a href="{{ route('square-pile') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
</div>
</div>
</div>
</div>
</div>
<div class="col-12 pt-3">
    <div class="product-card dieselHammer">
        <div class="row">
            <div class="col-md-4 justify-content-center">
                <img class="service-icon" src="assets/images/service/hydraulic.jpg">
            </div>
            <div class="col-md-7 ml-4">
                <div class="row pt-3">
                    <h2 class="text-bold text-xlarge text-blue">
                        Diesel Hammer Piling
                    </h2>
                </div>
                <div class="row ml30">
                    <p class="text-14 text-left">
                        Versatile and durable diesel hammer piling<br>rigs
                    </p>
                </div>
                <div class="row pb-4">
                    <a href="{{ route('square-pile') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-12 pt-3">
    <div class="product-card drillingServices">
        <div class="row">
            <div class="col-md-4 justify-content-center">
                <img class="service-icon" src="assets/images/service/hydraulic.jpg">
            </div>
            <div class="col-md-7 ml-4">
                <div class="row pt-3">
                    <h2 class="text-bold text-xlarge text-blue">
                        Drilling Services
                    </h2>
                </div>
                <div class="row ml30">
                    <p class="text-14 text-left">
                        We provide drilling services to break through<br>hard soil profiles
                    </p>
                </div>
                <div class="row pb-4">
                    <a href="{{ route('square-pile') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="col-12 col-md-6">
    <div class="row">
        <div class="col-12">
            <div class="product-card lateralLoading">
                <div class="row">
                    <div class="col-md-4 justify-content-center">
                        <img class="service-icon" src="assets/images/service/hydraulic.jpg">
                    </div>
                    <div class="col-md-7 ml-4">
                        <div class="row pt-3">
                            <h2 class="text-bold text-xlarge text-blue">
                                Lateral Loading Test
                            </h2>
                        </div>
                        <div class="row ml30">
                            <p class="text-14 text-left">
                                Measure the integrity of the piles<br>&nbsp;
                            </p>
                        </div>
                        <div class="row pb-4">
                            <a href="{{ route('square-pile') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 pt-3">
            <div class="product-card pileDriving">
                <div class="row">
                    <div class="col-md-4 justify-content-center">
                        <img class="service-icon" src="assets/images/service/hydraulic.jpg">
                    </div>
                    <div class="col-md-7 ml-4">
                        <div class="row pt-3">
                            <h2 class="text-bold text-xlarge text-blue">
                                Pile Driving Analysis Test
                            </h2>
                        </div>
                        <div class="row ml30">
                            <p class="text-14 text-left">
                                Accurately measure the pile bearing capacities<br>&nbsp;
                            </p>
                        </div>
                        <div class="row pb-4">
                            <a href="{{ route('square-pile') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 pt-3">
            <div class="product-card staticLoading">
                <div class="row">
                    <div class="col-md-4 justify-content-center">
                        <img class="service-icon" src="assets/images/service/hydraulic.jpg">
                    </div>
                    <div class="col-md-7 ml-4">
                        <div class="row pt-3">
                            <h2 class="text-bold text-xlarge text-blue">
                                Static Loading Test
                            </h2>
                        </div>
                        <div class="row ml30">
                            <p class="text-14 text-left">
                                Discover the bearing capacity of piles<br>&nbsp;
                            </p>
                        </div>
                        <div class="row pb-4">
                            <a href="{{ route('square-pile') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</section> --}}
{{-- <section id="product-category" class="bg-white-900 pt10 pb20 d-block d-lg-none">
    <div class="pb40" style="background-color: rgba(255, 255, 255, 0.9)">
        <div class="container pt-2">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col-12">
                            <div class="product-card hydraulicInjection">
                                <div class="row">
                                    <div class="ml-5">
                                        <div class="row pt-3">
                                            <h2 class="text-bold text-xlarge text-blue">
                                                Hydraulic Injection Piling
                                            </h2>
                                        </div>
                                        <div class="row ml30">
                                            <p class="text-14 text-left">
                                                Silent pile driving process
                                            </p>
                                        </div>
                                        <div class="row pb-4">
                                            <a href="{{ route('square-pile') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
</div>
</div>
</div>
</div>
</div>
<div class="col-12 pt-3">
    <div class="product-card dieselHammer">
        <div class="row">
            <div class="ml-5">
                <div class="row pt-3">
                    <h2 class="text-bold text-xlarge text-blue">
                        Diesel Hammer Piling
                    </h2>
                </div>
                <div class="row ml30">
                    <p class="text-14 text-left">
                        Versatile & durable hammer piling rigs
                    </p>
                </div>
                <div class="row pb-4">
                    <a href="{{ route('square-pile') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-12 pt-3">
    <div class="product-card drillingServices">
        <div class="row">
            <div class="ml-5">
                <div class="row pt-3">
                    <h2 class="text-bold text-xlarge text-blue">
                        Drilling Services
                    </h2>
                </div>
                <div class="row ml30">
                    <p class="text-14 text-left">
                        Break through hard soil profiles
                    </p>
                </div>
                <div class="row pb-4">
                    <a href="{{ route('square-pile') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="col-12 col-md-6">
    <div class="row">
        <div class="col-12">
            <div class="product-card lateralLoading">
                <div class="row">
                    <div class="ml-5">
                        <div class="row pt-3">
                            <h2 class="text-bold text-xlarge text-blue">
                                Lateral Loading Test
                            </h2>
                        </div>
                        <div class="row ml30">
                            <p class="text-14 text-left">
                                Measure the integrity of the piles
                            </p>
                        </div>
                        <div class="row pb-4">
                            <a href="{{ route('square-pile') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 pt-3">
            <div class="product-card pileDriving">
                <div class="row">
                    <div class="ml-5">
                        <div class="row pt-3">
                            <h2 class="text-bold text-xlarge text-blue">
                                Pile Driving Analysis Test
                            </h2>
                        </div>
                        <div class="row ml30">
                            <p class="text-14 text-left">
                                Accurately measure the pile bearing capacities
                            </p>
                        </div>
                        <div class="row pb-4">
                            <a href="{{ route('square-pile') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 pt-3">
            <div class="product-card staticLoading">
                <div class="row">
                    <div class="ml-5">
                        <div class="row pt-3">
                            <h2 class="text-bold text-xlarge text-blue">
                                Static Loading Test
                            </h2>
                        </div>
                        <div class="row ml30">
                            <p class="text-14 text-left">
                                Discover the bearing capacity of piles
                            </p>
                        </div>
                        <div class="row pb-4">
                            <a href="{{ route('square-pile') }}" class="btn btn-primary btn-rounded stretched-link">Lihat detail</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</section> --}}