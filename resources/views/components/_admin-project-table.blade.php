<section id="admin-project-table" class="cta pt-lg-5 pt-2 pb50">
    <div class="container rounded d-none d-lg-block mt-5 wow fadeInUp animated" style="background: rgb(27,47,136)">
        <div class="row ml-1 pt-4 pb-2">
            <p class="text-left text-32 text-bold text-white">
                PROJECT LIST
            </p>
        </div>
    </div>
    <div class="container d-block d-lg-none wow fadeInUp animated" style="background: rgb(27,47,136)">
        <div class="row ml-1 pt-4 pb-2">
            <p class="text-left text-32 text-bold text-white">
                PROJECT LIST
            </p>
        </div>
    </div>
    <div class="container wow fadeInUp animated">
        <div class="row pt-3 pb-3 mr-1" style="float: right">
            <a class="btn btn-primary" href="{{ route('project-form') }}" title="Add New">
                <i class="fas fa-plus"></i> Add New
            </a>
        </div>
        <table class="table table-responsive dataTable hover" id="admin">
            <thead>
                <tr>
                <th scope="col">No</th>
                <th scope="col">Project Name</th>
                <th scope="col">Client</th>
                <th scope="col">City</th>
                <th scope="col">Year</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                <th scope="row">1</th>
                <td>Gudang Absolute Mojotengah Gresik</td>
                <td>PT. Adhikarya Bumi Berkat Agung</td>
                <td>Gresik</td>
                <td>2016</td>
                <td><div class="badge bg-success">Active</div></td>
                <td class="text-center">
                    <a class="w3-btn btn-primary btn-rounded" href="{{ route('project-form') }}" title="Detail">
                        <i class="fas fa-search"></i>  Detail
                    </a>
                    <a class="w3-btn btn-warning btn-rounded" href="{{ route('project-form') }}" title="Edit">
                        <i class="fas fa-pencil-alt"></i>  Edit
                    </a>
                    <a class="w3-btn btn-danger btn-rounded eventInactive" href="" title="Inactive">
                        <i class="fas fa-times"></i>  Deactivate
                    </a>
                </td>
                </tr>
                <tr>
                <th scope="row">2</th>
                <td>Gold Vitel Basuki Rachmat Sby</td>
                <td>Ir. Sandi Febrianto</td>
                <td>Surabaya</td>
                <td>2016</td>
                                <td><div class="badge bg-success">Active</div></td>
                <td class="text-center">
                    <a class="w3-btn btn-primary btn-rounded" href="{{ route('project-form') }}" title="Detail">
                        <i class="fas fa-search"></i>  Detail
                    </a>
                    <a class="w3-btn btn-warning btn-rounded" href="{{ route('project-form') }}" title="Edit">
                        <i class="fas fa-pencil-alt"></i>  Edit
                    </a>
                    <a class="w3-btn btn-danger btn-rounded eventInactive" href="" title="Inactive">
                        <i class="fas fa-times"></i>  Deactivate
                    </a>
                </td>
                </tr>
                <tr>
                <th scope="row">3</th>
                <td>RS Aisyiyah Bojonegoro Kota</td>
                <td>RS Aisyiyah</td>
                <td>Bojonegoro</td>
                <td>2016</td>
                <td><div class="badge bg-danger text-white">Inactive</div></td>
                <td class="text-center">
                    <a class="w3-btn btn-primary btn-rounded" href="{{ route('project-form') }}" title="Detail">
                        <i class="fas fa-search"></i>  Detail
                    </a>
                    <a class="w3-btn btn-warning btn-rounded" href="{{ route('project-form') }}" title="Edit">
                        <i class="fas fa-pencil-alt"></i>  Edit
                    </a>
                    <a class="w3-btn btn-success btn-rounded eventInactive" href="" title="Inactive">
                        <i class="fas fa-check"></i>  Activate
                    </a>
                </td>
                </tr>
                <tr>
                <th scope="row">4</th>
                <td>Univ. Brawijaya Kediri</td>
                <td>PT. Afdolufaiz</td>
                <td>Kediri</td>
                <td>2016</td>
                <td><div class="badge bg-success">Active</div></td>
                <td class="text-center">
                    <a class="w3-btn btn-primary btn-rounded" href="{{ route('project-form') }}" title="Detail">
                        <i class="fas fa-search"></i>  Detail
                    </a>
                    <a class="w3-btn btn-warning btn-rounded" href="{{ route('project-form') }}" title="Edit">
                        <i class="fas fa-pencil-alt"></i>  Edit
                    </a>
                    <a class="w3-btn btn-danger btn-rounded eventInactive" href="" title="Inactive">
                        <i class="fas fa-times"></i>  Deactivate
                    </a>
                </td>
                </tr>
                <tr>
                <th scope="row">5</th>
                <td>Restorasi & Perluasan Ged. Eks Pengadilan Kediri</td>
                <td>PT. Ergates Citra Mandiri</td>
                <td>Kediri</td>
                <td>2016</td>
                <td><div class="badge bg-success">Active</div></td>
                <td class="text-center">
                    <a class="w3-btn btn-primary btn-rounded" href="{{ route('project-form') }}" title="Detail">
                        <i class="fas fa-search"></i>  Detail
                    </a>
                    <a class="w3-btn btn-warning btn-rounded" href="{{ route('project-form') }}" title="Edit">
                        <i class="fas fa-pencil-alt"></i>  Edit
                    </a>
                    <a class="w3-btn btn-danger btn-rounded eventInactive" href="" title="Inactive">
                        <i class="fas fa-times"></i>  Deactivate
                    </a>
                </td>
                </tr>
                <tr>
                <th scope="row">6</th>
                <td>Swissbel Darmo Centrum</td>
                <td>PT. PP Pracetak</td>
                <td>Surabaya</td>
                <td>2015</td>
                <td><div class="badge bg-success">Active</div></td>
                <td class="text-center">
                    <a class="w3-btn btn-primary btn-rounded" href="{{ route('project-form') }}" title="Detail">
                        <i class="fas fa-search"></i>  Detail
                    </a>
                    <a class="w3-btn btn-warning btn-rounded" href="{{ route('project-form') }}" title="Edit">
                        <i class="fas fa-pencil-alt"></i>  Edit
                    </a>
                    <a class="w3-btn btn-danger btn-rounded eventInactive" href="" title="Inactive">
                        <i class="fas fa-times"></i>  Deactivate
                    </a>
                </td>
                </tr>
                <tr>
                <th scope="row">7</th>
                <td>Gedung Mushola RAMP RS Jiwa</td>
                <td>PT. Waskita Karya</td>
                <td>Surabaya</td>
                <td>2015</td>
                <td><div class="badge bg-success">Active</div></td>
                <td class="text-center">
                    <a class="w3-btn btn-primary btn-rounded" href="{{ route('project-form') }}" title="Detail">
                        <i class="fas fa-search"></i>  Detail
                    </a>
                    <a class="w3-btn btn-warning btn-rounded" href="{{ route('project-form') }}" title="Edit">
                        <i class="fas fa-pencil-alt"></i>  Edit
                    </a>
                    <a class="w3-btn btn-danger btn-rounded eventInactive" href="" title="Inactive">
                        <i class="fas fa-times"></i>  Deactivate
                    </a>
                </td>
                </tr>
                <tr>
                <th scope="row">8</th>
                <td>Hotel Batika Surabaya</td>
                <td>PT. Nusa Raya Cipta Tbk</td>
                <td>Surabaya</td>
                <td>2015</td>
                <td><div class="badge bg-success">Active</div></td>
                <td class="text-center">
                    <a class="w3-btn btn-primary btn-rounded" href="{{ route('project-form') }}" title="Detail">
                        <i class="fas fa-search"></i>  Detail
                    </a>
                    <a class="w3-btn btn-warning btn-rounded" href="{{ route('project-form') }}" title="Edit">
                        <i class="fas fa-pencil-alt"></i>  Edit
                    </a>
                    <a class="w3-btn btn-danger btn-rounded eventInactive" href="" title="Inactive">
                        <i class="fas fa-times"></i>  Deactivate
                    </a>
                </td>
                </tr>
                <tr>
                <th scope="row">9</th>
                <td>Akademi AL Surabaya</td>
                <td>PT. Brantas Abipraya</td>
                <td>Surabaya</td>
                <td>2015</td>
                <td><div class="badge bg-success">Active</div></td>
                <td class="text-center">
                    <a class="w3-btn btn-primary btn-rounded" href="{{ route('project-form') }}" title="Detail">
                        <i class="fas fa-search"></i>  Detail
                    </a>
                    <a class="w3-btn btn-warning btn-rounded" href="{{ route('project-form') }}" title="Edit">
                        <i class="fas fa-pencil-alt"></i>  Edit
                    </a>
                    <a class="w3-btn btn-danger btn-rounded eventInactive" href="" title="Inactive">
                        <i class="fas fa-times"></i>  Deactivate
                    </a>
                </td>
                </tr>
                <tr>
                <th scope="row">10</th>
                <td>Indomarco Gresik</td>
                <td>PT. Nusa Raya Cipta</td>
                <td>Gresik</td>
                <td>2014</td>
                <td><div class="badge bg-success">Active</div></td>
                <td class="text-center">
                    <a class="w3-btn btn-primary btn-rounded" href="{{ route('project-form') }}" title="Detail">
                        <i class="fas fa-search"></i>  Detail
                    </a>
                    <a class="w3-btn btn-warning btn-rounded" href="{{ route('project-form') }}" title="Edit">
                        <i class="fas fa-pencil-alt"></i>  Edit
                    </a>
                    <a class="w3-btn btn-danger btn-rounded eventInactive" href="" title="Inactive">
                        <i class="fas fa-times"></i>  Deactivate
                    </a>
                </td>
                </tr>
                <tr>
                <th scope="row">11</th>
                <td>Krone Hotel</td>
                <td>PT. Mukti Adhi Sejahtera</td>
                <td>Surabaya</td>
                <td>2014</td>
                <td><div class="badge bg-success">Active</div></td>
                <td class="text-center">
                    <a class="w3-btn btn-primary btn-rounded" href="{{ route('project-form') }}" title="Detail">
                        <i class="fas fa-search"></i>  Detail
                    </a>
                    <a class="w3-btn btn-warning btn-rounded" href="{{ route('project-form') }}" title="Edit">
                        <i class="fas fa-pencil-alt"></i>  Edit
                    </a>
                    <a class="w3-btn btn-danger btn-rounded eventInactive" href="" title="Inactive">
                        <i class="fas fa-times"></i>  Deactivate
                    </a>
                </td>
                </tr>
                <tr>
                <th scope="row">12</th>
                <td>PT. Central Protein Prima</td>
                <td>PT. Bangun Karya Perkasa Jaya</td>
                <td>Surabaya</td>
                <td>2014</td>
                <td><div class="badge bg-success">Active</div></td>
                <td class="text-center">
                    <a class="w3-btn btn-primary btn-rounded" href="{{ route('project-form') }}" title="Detail">
                        <i class="fas fa-search"></i>  Detail
                    </a>
                    <a class="w3-btn btn-warning btn-rounded" href="{{ route('project-form') }}" title="Edit">
                        <i class="fas fa-pencil-alt"></i>  Edit
                    </a>
                    <a class="w3-btn btn-danger btn-rounded eventInactive" href="" title="Inactive">
                        <i class="fas fa-times"></i>  Deactivate
                    </a>
                </td>
                </tr>
                <tr>
                <th scope="row">13</th>
                <td>Java Cocoa Plant PT. Cargill</td>
                <td>PT. Tatamulia Nusantara Indah</td>
                <td>Gresik</td>
                <td>2014</td>
                <td><div class="badge bg-success">Active</div></td>
                <td class="text-center">
                    <a class="w3-btn btn-primary btn-rounded" href="{{ route('project-form') }}" title="Detail">
                        <i class="fas fa-search"></i>  Detail
                    </a>
                    <a class="w3-btn btn-warning btn-rounded" href="{{ route('project-form') }}" title="Edit">
                        <i class="fas fa-pencil-alt"></i>  Edit
                    </a>
                    <a class="w3-btn btn-danger btn-rounded eventInactive" href="" title="Inactive">
                        <i class="fas fa-times"></i>  Deactivate
                    </a>
                </td>
                </tr>
                <tr>
                <th scope="row">14</th>
                <td>Auto 2000</td>
                <td>PT. Dinamika Furindo Nusantara</td>
                <td>Surabaya</td>
                <td>2015</td>
                <td><div class="badge bg-success">Active</div></td>
                <td class="text-center">
                    <a class="w3-btn btn-primary btn-rounded" href="{{ route('project-form') }}" title="Detail">
                        <i class="fas fa-search"></i>  Detail
                    </a>
                    <a class="w3-btn btn-warning btn-rounded" href="{{ route('project-form') }}" title="Edit">
                        <i class="fas fa-pencil-alt"></i>  Edit
                    </a>
                    <a class="w3-btn btn-danger btn-rounded eventInactive" href="" title="Inactive">
                        <i class="fas fa-times"></i>  Deactivate
                    </a>
                </td>
                </tr>
                <tr>
                <th scope="row">15</th>
                <td>Hotel Brothers Solo</td>
                <td>PT. Brothers Graha Pratama</td>
                <td>Solo</td>
                <td>2015</td>
                <td><div class="badge bg-success">Active</div></td>
                <td class="text-center">
                    <a class="w3-btn btn-primary btn-rounded" href="{{ route('project-form') }}" title="Detail">
                        <i class="fas fa-search"></i>  Detail
                    </a>
                    <a class="w3-btn btn-warning btn-rounded" href="{{ route('project-form') }}" title="Edit">
                        <i class="fas fa-pencil-alt"></i>  Edit
                    </a>
                    <a class="w3-btn btn-danger btn-rounded eventInactive" href="" title="Inactive">
                        <i class="fas fa-times"></i>  Deactivate
                    </a>
                </td>
                </tr>
            </tbody>
        </table>
    </div>
</section>