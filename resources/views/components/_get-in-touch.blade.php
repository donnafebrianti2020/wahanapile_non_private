<section id="get-in-touch" class="cta pt40 pb40" style="background-color:white">
    <div class="container bg-get-in-touch rounded-lg d-block d-sm-none d-md-none va-middle" style="width: 90%; height:80%">
        <div class="row">
            <div class="col-sm-9 text-left mb-md-0 mb-3">
                <h2 class="black-shadow font-weight-bold text-white text-center wow zoomInRight animated"> 
                    Ingin menjalin kerjasama?
                <h2>
            </div>
            <div class="col-sm-3 text-center">
                <a href="https://wa.me/62895628095939" class="btn btn-rounded btn-lg btn-genesys wow zoomIn animated">
                    HUBUNGI KAMI
                </a>
            </div>
        </div>
    </div>
    <div class="container bg-get-in-touch rounded-lg d-none d-sm-block d-md-block va-middle">
        <div class="row">
            <div class="col-lg-12 col-xl-9 text-center mb-lg-3 mb-md-3">
                <h2 class="black-shadow font-weight-bold text-white wow zoomInRight animated"> 
                    Ingin menjalin kerjasama?
                <h2>
            </div>
            <div class="col-lg-12 col-xl-3 text-center">
                <a href="https://wa.me/62895628095939" class="btn btn-rounded btn-lg btn-genesys wow zoomIn animated">
                    HUBUNGI KAMI
                </a>
            </div>
        </div>
    </div>
</section>