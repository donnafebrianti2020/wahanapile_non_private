<section id="maps">
    <div class="container">
        <div class="row mb40">
            <div class="col-12">
                <div class="container">
                    <div class="title-heading1 mb40">
                        <h3 class="wow fadeInUp animated">Lokasi Kami</h3>
                    </div>
                </div>
                <div class="map-responsive wow zoomIn animated">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.388759281732!2d112.76797941461709!3d-7.310148673901642!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7faf95d395dfb%3A0xb9f513582a595877!2sPT%20WAHANA%20CIPTA%20CONCRETINDO!5e0!3m2!1sen!2sid!4v1644419795730!5m2!1sen!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>