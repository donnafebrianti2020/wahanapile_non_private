<section id="contact-form" class="bg-white-900 pb50 d-block d-lg-none">
    <div class="wow fadeInUp animated" style="background-image: url('assets/images/bg/blue-bg-3.png');">
            <div class="container">
                <div class="row text-white justify-content-center">
                    <p class="text-22 text-heavy pt40"> Contact Us </p>
                </div>
                <div class="row text-white justify-content-center">
                    <p class="text-16 text-center">
                        Isi form berikut dan Tim kami akan<br>segera mengubungi Anda.
                    </p>
                </div>
                <div class="row pl-1 pr-1 pt-2 pb-4">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <form>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    Subject<span>*</span>
                                                </label>
                                                <input type="text" name="subject" class="form-control" required="" placeholder="Insert subject">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    Name<span>*</span>
                                                </label>
                                                <input type="text" name="name" class="form-control" required="" placeholder="Insert name">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    Email<span>*</span>
                                                </label>
                                                <input type="email" name="email" class="form-control" required="" placeholder="Insert email address">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    Phone<span>*</span>
                                                </label>
                                                <input type="number" name="phone" class="form-control" required="" placeholder="Insert phone number">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    Message<span>*</span>
                                                </label>
                                                <textarea class="form-control" name="message" placeholder="Insert message here" rows="5" required=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row pt-3">
                                        <div class="col-md-12">
                                            <button type="submit" name="submit" class="btn btn-primary text-center float-right rounded-container">
                                                Send
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pb-3">
                    <div class="col-12 col-md-4 justify-content-center">
                        <p class="text-white text-16 text-center">
                            <b>Whatsapp</b> <br>
                            0895 628 095 939
                        </p>
                    </div>
                    <div class="col-12 col-md-4 justify-content-center">
                        <p class="text-white text-16 text-center">
                            <b>Phone</b> <br>
                            (031) 879 1555, 879 2655
                        </p>
                    </div>
                    <div class="col-12 col-md-4 justify-content-center">
                        <p class="text-white text-16 text-center">
                            <b>Email</b> <br>
                            info@wahanaconcrete.com
                        </p>
                    </div>
                </div>
            </div>
    </div>
</section>
<section id="contact-form" class="bg-white-900 pt100 pb80 d-none d-lg-block">
    <div class="container rounded-container mt-3 wow zoomIn animated" style="background-image: url('assets/images/bg/blue-bg-2.png');">
        <div class="row">
            <div class="col-md-4 pl-5 pl-5">
                <div class="row ml-1">
                    <h2 class="text-white text-heavy pt80"> Contact Us </h2>
                </div>
                <div class="row ml-1">
                    <p class="text-white text-16">
                        Isi form berikut dan Tim kami akan <br>mengubungi Anda segera.
                    </p>
                </div>
                <div class="row pt-5">
                    <div class="col-2">
                        <a href="https://wa.me/62895628095939" class="social-icon si-dark si-gray-round si-colored-google-plus" target="_blank" rel="noopener"> <i class="fab fa-whatsapp"></i> <i class="fab fa-whatsapp"></i> </a>
                    </div>
                    <div class="col-7">
                        <p class="text-white text-16 va-middle">
                            <b>Whatsapp</b> <br>
                            0895 628 095 939
                        </p>
                    </div>
                </div>
                <div class="row pt-1">
                    <div class="col-2">
                        <a href="" class="social-icon si-dark si-gray-round si-colored-google-plus"> <i class="fas fa-phone fa-xs"></i> <i class="fas fa-phone fa-xs"></i> </a>
                    </div>
                    <div class="col-7">
                        <p class="text-white text-16 va-middle">
                            <b>Phone</b> <br>
                            (031) 879 1555 <br>
                            (031) 879 2655
                        </p>
                    </div>
                </div>
                <div class="row pt-1">
                    <div class="col-2">
                        <a href="mailto:info@wahanaconcrete.com" class="social-icon si-dark si-gray-round si-colored-google-plus" target="_blank" rel="noopener"> <i class="fas fa-envelope fa-xs"></i> <i class="fas fa-envelope fa-xs"></i> </a>
                    </div>
                    <div class="col-7">
                        <p class="text-white text-16 va-middle">
                            <b>Email</b> <br>
                            info@wahanaconcrete.com
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-8 pt50 pb50">
                <div class="container">
                    <div class="card">
                        <div class="card-body">
                            <form>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">
                                                Subject<span>*</span>
                                            </label>
                                            <input type="text" name="subject" class="form-control" required="" placeholder="Insert subject">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">
                                                Name<span>*</span>
                                            </label>
                                            <input type="text" name="name" class="form-control" required="" placeholder="Insert name">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">
                                                Email<span>*</span>
                                            </label>
                                            <input type="email" name="email" class="form-control" required="" placeholder="Insert email address">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">
                                                Phone<span>*</span>
                                            </label>
                                            <input type="number" name="phone" class="form-control" required="" placeholder="Insert phone number">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">
                                                Message<span>*</span>
                                            </label>
                                            <textarea class="form-control" name="message" placeholder="Insert message here" rows="5" required=""></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row pt-3">
                                    <div class="col-md-12">
                                        <button type="submit" name="submit" class="btn btn-primary text-center float-right rounded-container">
                                            Send
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>
</section>