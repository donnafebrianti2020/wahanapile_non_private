<section id="square-pile-banner" class="bg-white-900 pt100 d-none d-lg-block">
    <div class="container no-padding">
            <div class="rounded-lg" style='background-image: url("assets/images/product/square-pile/square-pile-1.jpg")'>
                <div class="col-12">
                    <div class="row rounded-lg" style="background-image: linear-gradient(rgba(24,27,136,0.75), rgba(24,27,136,0.45)">
                        <div class="col-md-8 pt-4 justify-content-center">
                            <p class="shadow-title pt-4 pl-5 wow fadeInUp animated" style="text-align: left !important;">Square Pile</p>
                        </div>
                        <div class="col-md-4 pt-4">
                            <ul class="text-li text-white">
                                <li class="wow fadeInUp animated"> 20x20cm - 45x45cm dimension square piles </li>
                                <li class="wow fadeInUp animated"> Up to 4000 meters of total square pile production capacity daily </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="square-pile-banner" class="bg-white-900 pt10 d-none d-lg-none d-md-block">
    <div class="container no-padding">
            <div class="rounded-lg" style='background-image: url("assets/images/product/square-pile/square-pile-1.jpg")'>
                <div class="col-12">
                    <div class="row rounded-lg" style="background-image: linear-gradient(rgba(24,27,136,0.75), rgba(24,27,136,0.45)">
                        <div class="col-md-7 pt-4 justify-content-center">
                            <p class="shadow-title pt-4 pl-5 wow fadeInUp animated" style="text-align: left !important;">Square Pile</p>
                        </div>
                        <div class="col-md-5 pt-4">
                            <ul class="text-li text-white">
                                <li class="wow fadeInUp animated"> 20x20cm - 45x45cm dimension square piles </li>
                                <li class="wow fadeInUp animated"> Up to 4000 meters of total square pile production capacity daily </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="square-pile-banner" class="bg-white-900 pt10 d-block d-lg-none d-md-none">
    <div class="container no-padding">
            <div class="" style='background-image: url("assets/images/product/square-pile/square-pile-1.jpg")'>
                <div class="col-12">
                    <div class="row pt-4 justify-content-center" style="background-image: linear-gradient(rgba(24,27,136,0.75), rgba(24,27,136,0.45)">
                        <div class="row">
                            <p class="shadow-title pt-4 text-center wow fadeInUp animated">Square Pile</p>
                        </div>
                        <div class="row justify-content-center pt-4">
                            <p class="text-white text-14 text-center wow fadeInUp animated">
                                20x20cm - 45x45cm dimension square piles<br>
                                Up to 4000 meters of total square pile<br>
                                production capacity daily
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>