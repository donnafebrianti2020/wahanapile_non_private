<section id="advantage" class="bg-white-900 pt20 pb50">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <h2 class="text-center wow fadeInUp animated">Our Competitive Advantage</h2>
            <p class="text-center pl-3 pr-3 wow fadeInUp animated">
                Savvy at both aspects of its product and services allows us to complete projects quickly
                while delivering satisfactory services.
            </p>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-4 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center">
                    <div class="row justify-content-center align-items-center">
                        <img class="advantage-icon wow zoomIn animated" src="assets/images/icon/quality.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center">
                        <p class="text-18 text-blue text-bold text-center wow fadeInUp animated"> Quality </p>
                    </div>
                    <div class="row justify-content-center align-items-center">
                        <p class="text-16 text-justify wow fadeInUp animated">
                            The concrete products which we offers are designed to be of an exquisite quality.
                            To ensure the hardness and elasticity of the concrete we made sure to use the best
                            quality of readymixes, steel, and additives. Our concrete products will harden up to
                            500- K in no more than 2 weeks.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center">
                    <div class="row justify-content-center align-items-center">
                        <img class="advantage-icon wow zoomIn animated" src="assets/images/icon/variance.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center">
                        <p class="text-18 text-blue text-bold text-center wow fadeInUp animated"> Variance </p>
                    </div>
                    <div class="row justify-content-center align-items-center">
                        <p class="text-16 text-justify wow fadeInUp animated">
                            Our concrete can always be altered to meet the customers needs.
                            Our experience allows us to fabricate products which fulfills the specifications
                            requested by consultants and contractors alike. Be it stronger or more efficient
                            our team will deliver the products of the finest quality.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center">
                    <div class="row justify-content-center align-items-center">
                        <img class="advantage-icon wow zoomIn animated" src="assets/images/icon/delivery.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center">
                        <p class="text-18 text-blue text-bold text-center wow fadeInUp animated"> Delivery </p>
                    </div>
                    <div class="row justify-content-center align-items-center">
                        <p class="text-16 text-justify wow fadeInUp animated">
                            We also cater to the needs of our customers by delivering our concrete products
                            to their projects & sites. Our vast armada allows fast & timely delivery.
                            Selling both piling products & services allows our customers to save precious time
                            and be efficient.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>