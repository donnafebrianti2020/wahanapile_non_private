<section id="home-profile" class="bg-white-900 bg-parallax" style='background-image: url("assets/images/homeprofile/homeprofile-3.jpeg"); height: 750px'>
    <div class="container d-block d-sm-none" style="background-color: rgba(255, 255, 255, 0.85); height:100%">
        <div class="row">
            <div class="col-sm-12">
                <div class="row justify-content-center">
                    <img class="wow fadeInUp animated" src="{{ asset('assets/images/logo/wahana/wahana2.png') }}" style="height: 120px;">
                </div>
                <div class="row justify-content-center">
                    <h3 class="text-bold wow fadeInUp animated">
                        PT WAHANA CIPTA CONCRETINDO
                    </h3>
                </div>
                <div class="row justify-content-center">
                    <p class="text-14 text-blue half-line wow fadeInUp animated">
                        PRECAST - PRESTRESSED CONCRETE PRODUCTS
                    </p>
                </div>
                <div class="row justify-content-center pl-3 pr-3 pt-2">
                    <p class="text-sm wow fadeInUp animated">
                        <strong> PT Wahana Cipta Concretindo </strong> merupakan salah satu perusahaan yang berada di bawah naungan
                        Wahana Group. Perusahaan ini bergerak di industri beton pracetak (Precast Concrete).
                        Selama lebih dari 30 tahun, PT Wahana Cipta Concretindo telah memproduksi tiang pancang,
                        pagar beton, girder jembatan dan segala jenis beton pracetak untuk memenuhi kebutuhan
                        konsumen.
                    </p>
                </div>
                <div class="row justify-content-center pl-3 pr-3 pb-1">
                    <ul class="text-li wow fadeInUp animated">
                        <li> Tiang Pancang Kotak </li>
                        <li> Tiang Pancang Segitiga </li>
                        <li> Sheet Pile </li>
                        <li> Girder Jembatan </li>
                        <li> Pagar Beton & Facade </li>
                        <li> Balok & Kolom Pracetak </li>
                        <li> Saluran / U ditch </li>
                        <li> dan berbagai produk beton lainnya </li> 
                    </ul>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="container d-none d-sm-block">
        <div class="row mt-5 mb-5">
            <div class="col-lg-4 mt-5 mb-5 ml-5 no-event d-md-none d-lg-block">
                <img src="{{ asset('assets/images/homeprofile/homeprofileimage.jpeg') }}" style="height: 550px;">
            </div>
            <div class="col-lg-6 col-md-12 mt-5 mb-5 bg-white">
                <div class="row ml-5 mt-5">
                    <h3 class="text-bold wow fadeInUp animated">
                        PT WAHANA CIPTA CONCRETINDO
                    </h3>
                </div>
                <div class="row ml-5 mr-5">
                    <h3 class="tagline-text text-blue wow fadeInUp animated">
                        PRECAST - PRESTRESSED CONCRETE PRODUCTS
                    </h3>
                </div>
                <div class="row ml-5 mt-4 mr-5">
                    <h3 class="text-content wow fadeInUp animated">
                        PT Wahana Cipta Concretindo merupakan salah satu perusahaan yang berada di bawah naungan
                        Wahana Group. Perusahaan ini bergerak di industri beton pracetak (Precast Concrete).
                        Selama lebih dari 30 tahun, PT Wahana Cipta Concretindo telah memproduksi tiang pancang,
                        pagar beton, girder jembatan dan segala jenis beton pracetak untuk memenuhi kebutuhan
                        konsumen.
                    </h3>
                </div>
                <div class="row ml-4 mt-3 mr-5 pb-md-3">
                    <h3 class="text-content wow fadeInUp animated">
                        <ul>
                            <li> Tiang Pancang Kotak </li>
                            <li> Tiang Pancang Segitiga </li>
                            <li> Sheet Pile </li>
                            <li> Girder Jembatan </li>
                            <li> Pagar Beton & Facade </li>
                            <li> Balok & Kolom Pracetak </li>
                            <li> Saluran / U ditch </li>
                            <li> dan berbagai produk beton lainnya </li> 
                        </ul>
                    </h3>
                </div>
            </div>
        </div>
    </div>
</section>