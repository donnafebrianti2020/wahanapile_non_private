<section id="product-category" class="bg-white-900 pt100 pb20 d-none d-lg-block">
    <div class="pb40" style="background-color: rgba(255, 255, 255, 0.9)">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-6">
                    <div class="product-card squarePile wow zoomIn animated">
                        <div class="row justify-content-center pt-4 pb-2">
                            <img class="product-category" src="assets/images/product/square-pile/square-pile-4.jpg" draggable="false">
                        </div>
                        <div class="row justify-content-center pt-3">
                            <h2 class="text-bold text-xlarge text-blue">
                                Square Pile
                            </h2>
                        </div>
                        <div class="row justify-content-center pt-1 ml30 mr30">
                            <p class="text-14 text-center">
                                Up to 4000 meters of total square pile <br>production capacity daily
                            </p>
                        </div>
                        <div class="row justify-content-center pb-4">
                            <a href="{{ route('square-pile') }}" class="btn btn-primary btn-rounded stretched-link">LIHAT PRODUK</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 pr-4">
                    <div class="product-card sheetPile wow zoomIn animated">
                        <div class="row justify-content-center pt-4 pb-2">
                            <img class="product-category" src="assets/images/product/sheet-pile/sheet-pile-3.jpg" draggable="false">
                        </div>
                        <div class="row justify-content-center pt-3">
                            <h2 class="text-bold text-xlarge text-blue">
                                Sheet Pile
                            </h2>
                        </div>
                        <div class="row justify-content-center pt-1 ml30 mr30">
                            <p class="text-14 text-center">
                                Over 1000 mtrs of total sheet pile and concrete wall <br>production capacity daily
                            </p>
                        </div>
                        <div class="row justify-content-center pb-4">
                            <a href="{{ route('sheet-pile') }}" class="btn btn-primary btn-rounded stretched-link">LIHAT PRODUK</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="product-category" class="bg-white-900 pt20 pb20 d-block d-lg-none">
    <div class="pb40" style="background-color: rgba(255, 255, 255, 0.9)">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-6">
                    <div class="product-card squarePile wow zoomIn animated">
                        <div class="row justify-content-center pt-4 pb-2">
                            <img class="product-category-sm" src="assets/images/product/square-pile/square-pile-4.jpg" draggable="false">
                        </div>
                        <div class="row justify-content-center pt-3">
                            <h2 class="text-bold text-xlarge text-blue">
                                Square Pile
                            </h2>
                        </div>
                        <div class="row justify-content-center pt-1 ml30 mr30">
                            <p class="text-14 text-center">
                                Up to 4000 meters of total square pile <br>production capacity daily
                            </p>
                        </div>
                        <div class="row justify-content-center pb-4">
                            <a href="{{ route('square-pile') }}" class="btn btn-primary btn-rounded stretched-link">LIHAT PRODUK</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 pt-5 pt-md-0">
                    <div class="product-card sheetPile wow zoomIn animated">
                        <div class="row justify-content-center pt-4 pb-2">
                            <img class="product-category-sm" src="assets/images/product/sheet-pile/sheet-pile-3.jpg" draggable="false">
                        </div>
                        <div class="row justify-content-center pt-3">
                            <h2 class="text-bold text-xlarge text-blue">
                                Sheet Pile
                            </h2>
                        </div>
                        <div class="row justify-content-center pt-1 ml30 mr30">
                            <p class="text-14 text-center">
                                Over 1000 mtrs of total sheet pile and<br>concrete wall production capacity daily
                            </p>
                        </div>
                        <div class="row justify-content-center pb-4">
                            <a href="{{ route('sheet-pile') }}" class="btn btn-primary btn-rounded stretched-link">LIHAT PRODUK</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>