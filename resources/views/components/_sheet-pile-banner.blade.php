<section id="sheet-pile-banner" class="bg-white-900 pt100 d-none d-lg-block">
    <div class="container no-padding">
            <div class="rounded-lg" style='background-image: url("assets/images/product/sheet-pile/sheet-pile-2.jpg")'>
                <div class="col-12">
                    <div class="row rounded-lg" style="background-image: linear-gradient(rgba(24,27,136,0.75), rgba(24,27,136,0.45)">
                        <div class="col-md-8 pt-4 justify-content-center">
                            <p class="shadow-title pt-4 pl-5 wow fadeInUp animated" style="text-align: left !important;">Sheet Pile</p>
                        </div>
                        <div class="col-md-4 pt-4">
                            <ul class="text-li text-white">
                                <li class="wow fadeInUp animated"> Flat & corrugated sheet pile, varying concrete walls sizes </li>
                                <li class="wow fadeInUp animated"> Over 1000 mtrs of total sheet pile and concrete wall production capacity daily </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="sheet-pile-banner" class="bg-white-900 pt10 d-none d-lg-none d-md-block">
    <div class="container no-padding">
            <div class="rounded-lg" style='background-image: url("assets/images/product/sheet-pile/sheet-pile-2.jpg")'>
                <div class="col-12">
                    <div class="row rounded-lg" style="background-image: linear-gradient(rgba(24,27,136,0.75), rgba(24,27,136,0.45)">
                        <div class="col-md-7 pt-5 justify-content-center">
                            <p class="shadow-title pt-4 pl-5 wow fadeInUp animated" style="text-align: left !important;">Sheet Pile</p>
                        </div>
                        <div class="col-md-5 pt-4">
                            <ul class="text-li text-white">
                                <li class="wow fadeInUp animated"> Flat & corrugated sheet pile, varying concrete walls sizes </li>
                                <li class="wow fadeInUp animated"> Over 1000 mtrs of total sheet pile and concrete wall production capacity daily </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="sheet-pile-banner" class="bg-white-900 pt10 d-block d-lg-none d-md-none">
    <div class="container no-padding">
            <div class="" style='background-image: url("assets/images/product/sheet-pile/sheet-pile-2.jpg")'>
                <div class="col-12">
                    <div class="row pt-3 justify-content-center" style="background-image: linear-gradient(rgba(24,27,136,0.75), rgba(24,27,136,0.45)">
                        <div class="row">
                            <p class="shadow-title pt-4 pl-2 pr-2 text-center wow fadeInUp animated">Sheet Pile</p>
                        </div>
                        <div class="row justify-content-center pt-3">
                            <p class="text-white text-14 text-center wow fadeInUp animated">
                                Flat & corrugated sheet pile,<br>
                                concrete walls sizes<br>
                                Over 1000 mtrs of total sheet pile and<br>
                                concrete wall production capacity daily
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>