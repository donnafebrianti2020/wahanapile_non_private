<section id="project-form" class="bg-white-900 pt50 pb50 bg-img">
    <div class="container">
        <div class="row justify-content-center pt80 d-none d-lg-block">
            &nbsp;
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card wow zoomIn animated">
                    <div class="card-header">{{ __('Project') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('dashboard') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('Project Name') }}</label>

                                <div class="col-md-8">
                                    <input id="name" type="text" class="form-control" name="name" required autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="client" class="col-md-3 col-form-label text-md-right">{{ __('Client') }}</label>

                                <div class="col-md-8">
                                    <input id="client" type="text" class="form-control" name="client" required autofocus>

                                    @error('client')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="city" class="col-md-3 col-form-label text-md-right">{{ __('City') }}</label>

                                <div class="col-md-8">
                                    <input id="city" type="text" class="form-control" name="city" required autofocus>

                                    @error('city')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="year" class="col-md-3 col-form-label text-md-right">{{ __('Year') }}</label>

                                <div class="col-md-8">
                                    <select id="year" type="number" class="form-control @error('year') is-invalid @enderror" name="year" required autofocus>
                                        <option selected>2022</option>
                                        <option>2021</option>
                                        <option>2020</option>
                                        <option>2019</option>
                                        <option>2018</option>
                                        <option>2017</option>
                                        <option>2016</option>
                                        <option>2015</option>
                                        <option>2014</option>
                                    </select>

                                    @error('year')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="status" class="col-md-3 col-form-label text-md-right">{{ __('Status') }}</label>

                                <div class="col-md-8">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="status" id="status" checked>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Active') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="status" id="status">

                                        <label class="form-check-label" for="remember">
                                            {{ __('Inactive') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary float-right">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>