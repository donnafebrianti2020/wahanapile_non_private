<section id="hydraulic-injection" class="bg-white-900 pt100 pb20 d-none d-lg-block">
    <div class="container">
            <div class="row align-items-center pt-2 pb-5">
                <div class="col-5 d-none d-md-block va-middle">
                    <img class="content-image wow fadeInUp animated" src="assets/images/service/hydraulic/hydraulic-1.jpeg" draggable="false">
                </div>
                <div class="col-7 d-none d-md-block pl-5 pt-5 align-items-center">
                    <div class="row align-items-center">
                        <p class="text-32 text-black text-bold text-left wow fadeInUp animated">
                            HYDRAULIC INJECTION PILING
                        </p>
                    </div>
                    <div class="row align-items-center">
                        <p class="text-blue text-left half-line text-22 wow fadeInUp animated">
                            Silent Piling Solution
                        </p>
                    </div>
                    <div class="row pt-2 align-items-center">
                        <p class="text-left text-14 wow fadeInUp animated">
                            The hydraulic Injection piling is one of Wahana’s most sought after services.
                            Offering a wide array of Hydraulic Injection machinery, Wahana is able to cater
                            to the needs of it’s customers. We offer From the most compact 120 to 800 Tones
                            driving rig nowadays.
                        </p>
                    </div>
                    <div class="row align-items-center">
                        <p class="text-left text-14 half-line wow fadeInUp animated">
                            Hydraulic Injection Piling is eligible for the piling of :
                        </p>
                        <ul class="text-14 wow fadeInUp animated">
                            <li> Spun piles from diameter 40 to 80 piles </li>
                            <li> Steel piles </li>
                            <li> Square piles from 20x20 dimension pile to 50x50 piles </li>
                            <li> Sheet and corrugated piles </li>
                            <li> Triangle piles </li>
                        </ul>
                    </div>
                    <div class="row pt-4">
                        <div class="col-md-3">
                            <img class="icon-image wow fadeInUp animated" src="assets/images/service/hydraulic/silent.png" draggable="false">
                            <p class="text-center text-14 half-line wow fadeInUp animated">
                                Silent
                            </p>
                        </div>
                        <div class="col-md-3">
                            <img class="icon-image wow fadeInUp animated" src="assets/images/service/hydraulic/noisefree.png" draggable="false">
                            <p class="text-center text-14 half-line wow fadeInUp animated">
                                Noise Free
                            </p>
                        </div>
                        <div class="col-md-3">
                            <img class="icon-image wow fadeInUp animated" src="assets/images/service/hydraulic/environmentallyfriendly.png" draggable="false">
                            <p class="text-center text-14 half-line wow fadeInUp animated">
                                Environmentally Friendly
                            </p>
                        </div>
                        <div class="col-md-3">
                            <img class="icon-image wow fadeInUp animated" src="assets/images/service/hydraulic/pollutionfree.png" draggable="false">
                            <p class="text-center text-14 half-line wow fadeInUp animated">
                                Pollution Free
                            </p>
                        </div>
                </div>
            </div>
        </div>
</section>
<section id="hydraulic-injection" class="bg-white-900 pt30 pb20 d-none d-lg-none d-md-block">
    <div class="container">
        <div class="row align-items-center justify-content-center pb-5 pr-2">
            <div class="col-12 d-none d-md-block align-items-center justify-content-center pb-4">
                <div class="row align-items-center justify-content-center">
                    <p class="text-32 text-black text-bold text-center half-line wow fadeInUp animated">
                        HYDRAULIC INJECTION PILING
                    </p>
                </div>
                <div class="row align-items-center justify-content-center">
                    <p class="text-blue text-center half-line text-22 wow fadeInUp animated">
                        Silent Piling Solution
                    </p>
                </div>
            </div>
            <div class="col-6 d-none d-md-block pl-5 pr-3 align-items-center">
                <div class="row pt-2 align-items-center">
                    <p class="text-left text-14 wow fadeInUp animated">
                        The hydraulic Injection piling is one of Wahana’s most sought after services.
                        Offering a wide array of Hydraulic Injection machinery, Wahana is able to cater
                        to the needs of it’s customers. We offer From the most compact 120 to 800 Tones
                        driving rig nowadays.
                    </p>
                </div>
                <div class="row align-items-center wow fadeInUp animated">
                    <p class="text-left text-14 half-line">
                        Hydraulic Injection Piling is eligible for the piling of :
                    </p>
                    <ul class="text-14">
                        <li> Spun piles from diameter 40 to 80 piles </li>
                        <li> Steel piles </li>
                        <li> Square piles from 20x20 dimension pile to 50x50 piles </li>
                        <li> Sheet and corrugated piles </li>
                        <li> Triangle piles </li>
                    </ul>
                </div>
            </div>
            <div class="col-6 d-none d-md-block va-middle pb-4">
                    <img class="content-image wow fadeInUp animated" src="assets/images/service/hydraulic/hydraulic-1.jpeg" draggable="false">
                </div>
            <div class="col-12 d-none d-md-block align-items-center">
                <div class="row pt-4">
                    <div class="col-md-3">
                        <img class="icon-image wow fadeInUp animated" src="assets/images/service/hydraulic/silent.png" draggable="false">
                        <p class="text-center text-14 half-line wow fadeInUp animated">
                            Silent
                        </p>
                    </div>
                    <div class="col-md-3">
                        <img class="icon-image wow fadeInUp animated" src="assets/images/service/hydraulic/noisefree.png" draggable="false">
                        <p class="text-center text-14 half-line wow fadeInUp animated">
                            Noise Free
                        </p>
                    </div>
                    <div class="col-md-3">
                        <img class="icon-image wow fadeInUp animated" src="assets/images/service/hydraulic/environmentallyfriendly.png" draggable="false">
                        <p class="text-center text-14 half-line wow fadeInUp animated">
                            Environmentally Friendly
                        </p>
                    </div>
                    <div class="col-md-3">
                        <img class="icon-image wow fadeInUp animated" src="assets/images/service/hydraulic/pollutionfree.png" draggable="false">
                        <p class="text-center text-14 half-line wow fadeInUp animated">
                            Pollution Free
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="hydraulic-injection" class="bg-white-900 pt30 pb20 d-block d-lg-none d-md-none">
    <div class="container">
        <div class="row align-items-center justify-content-center pr-2">
            <div class="col-12 align-items-center justify-content-center">
                <div class="row align-items-center justify-content-center">
                    <p class="text-26 text-black text-bold text-center half-line wow fadeInUp animated">
                        HYDRAULIC INJECTION PILING
                    </p>
                </div>
                <div class="row align-items-center justify-content-center">
                    <p class="text-blue text-center half-line text-22 wow fadeInUp animated">
                        Silent Piling Solution
                    </p>
                </div>
            </div>
        </div>
        <div class="row align-items-center justify-content-center pt-2 ml-2 mr-2 pb-3">
            <div class="col-12 align-items-center">
                <div class="row pt-2 align-items-center">
                    <p class="text-justify text-14 wow fadeInUp animated">
                        The hydraulic Injection piling is one of Wahana’s most sought after services.
                        Offering a wide array of Hydraulic Injection machinery, Wahana is able to cater
                        to the needs of it’s customers. We offer From the most compact 120 to 800 Tones
                        driving rig nowadays.
                    </p>
                </div>
                <div class="row align-items-center wow fadeInUp animated">
                    <p class="text-left text-14 half-line">
                        Hydraulic Injection Piling is eligible for the piling of :
                    </p>
                    <ul class="text-14">
                        <li> Spun piles from diameter 40 to 80 piles </li>
                        <li> Steel piles </li>
                        <li> Square piles from 20x20 dimension pile to 50x50 piles </li>
                        <li> Sheet and corrugated piles </li>
                        <li> Triangle piles </li>
                    </ul>
                </div>
            </div>
            <div class="col-12 align-items-center">
                <div class="row pt-4">
                    <div class="col-6">
                        <img class="icon-image wow fadeInUp animated" src="assets/images/service/hydraulic/silent.png" draggable="false">
                        <p class="text-center text-14 half-line wow fadeInUp animated">
                            Silent
                        </p>
                    </div>
                    <div class="col-6">
                        <img class="icon-image wow fadeInUp animated" src="assets/images/service/hydraulic/noisefree.png" draggable="false">
                        <p class="text-center text-14 half-line wow fadeInUp animated">
                            Noise Free
                        </p>
                    </div>
                </div>
                <div class="row pt-1">
                    <div class="col-6">
                        <img class="icon-image wow fadeInUp animated" src="assets/images/service/hydraulic/environmentallyfriendly.png" draggable="false">
                        <p class="text-center text-14 half-line wow fadeInUp animated">
                            Environmentally Friendly
                        </p>
                    </div>
                    <div class="col-6">
                        <img class="icon-image wow fadeInUp animated" src="assets/images/service/hydraulic/pollutionfree.png" draggable="false">
                        <p class="text-center text-14 half-line wow fadeInUp animated">
                            Pollution Free
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="hydraulic-target" class="bg-white-900 pt20 pb50 d-none d-lg-block">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <p class="text-center text-22 wow fadeInUp animated">Hydraulic Injection Piling Rigs are ideal for</p>
        </div>
        <div class="row justify-content-center pt-3">
            <div class="col-md-2 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-1.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Medium Sized Residences </p>
                    </div>
                </div>
            </div>
            <div class="col-md-2 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-2.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Large Sized Residences </p>
                    </div>
                </div>
            </div>
            <div class="col-md-2 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-3.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Apartments </p>
                    </div>
                </div>
            </div>
            <div class="col-md-2 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-4.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Shops </p>
                    </div>
                </div>
            </div>
            <div class="col-md-2 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-5.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Malls </p>
                    </div>
                </div>
            </div>
            <div class="col-md-2 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-6.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Populated & Crowded Districts </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="hydraulic-target" class="bg-white-900 pb50 d-none d-lg-none d-md-block">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <p class="text-center text-22 wow fadeInUp animated">Hydraulic Injection Piling Rigs are ideal for</p>
        </div>
        <div class="row justify-content-center pt-3">
            <div class="col-md-4 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-1.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Medium Sized Residences </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-2.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Large Sized Residences </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-3.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Apartments </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center pt-3">
            <div class="col-md-4 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-4.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Shops </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-5.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Malls </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-6.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Populated & Crowded Districts </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="hydraulic-target" class="bg-white-900 pt20 pb50 d-block d-lg-none d-md-none">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <p class="text-center text-22 wow fadeInUp animated">Hydraulic Injection Piling Rigs<br>are ideal for</p>
        </div>
        <div class="row justify-content-center pt-3">
            <div class="col-6 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-1.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Medium Sized Residences </p>
                    </div>
                </div>
            </div>
            <div class="col-6 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-2.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Large Sized Residences </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center pt-3">
            <div class="col-6 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-3.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Apartments </p>
                    </div>
                </div>
            </div>
            <div class="col-6 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-4.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Shops </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center pt-3">
            <div class="col-6 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-5.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Malls </p>
                    </div>
                </div>
            </div>
            <div class="col-6 justify-content-center align-items-center">
                <div class="container justify-content-center align-items-center wow zoomIn animated">
                    <div class="row justify-content-center align-items-center">
                        <img class="icon-image "src="assets/images/service/hydraulic/hydraulic-icon-6.png" draggable="false">
                    </div>
                    <div class="row justify-content-center align-items-center pt-1">
                        <p class="text-16 text-bold text-center"> Populated & Crowded Districts </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>