<section id="proyek" class="bg-white-900 pt30 pb40">
    <div class="container">
        <div class="title-heading1 mb40 wow fadeInUp animated">
            <h3>Proyek Kami</h3>
        </div>
    </div>
    <div class="">
        <div class="image-grid">
            <div class="row">
                <div class="column">
                    <div class="image-container mt-3 wow zoomIn animated">
                        <img src="assets/images/project/project-1.jpg" style="width: 100%;">
                        <div class="overlay">
                            <div class="image-text">
                                Lorem ipsum dolor sub amet
                            </div>
                        </div>
                    </div>
                    <div class="image-container mt-3 wow zoomIn animated">
                        <img src="assets/images/project/project-5.jpg" style="width: 100%;">
                        <div class="overlay">
                            <div class="image-text">
                                Lorem ipsum dolor sub amet
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column d-none d-sm-block">
                    <div class="image-container mt-3 wow zoomIn animated">
                        <img src="assets/images/project/project-2.jpg" style="width: 100%;">
                        <div class="overlay">
                            <div class="image-text">
                                Lorem ipsum dolor sub amet
                            </div>
                        </div>
                    </div>
                    <div class="image-container mt-3 wow zoomIn animated">
                        <img src="assets/images/project/project-6.jpg" style="width: 100%;">
                        <div class="overlay">
                            <div class="image-text">
                                Lorem ipsum dolor sub amet
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column d-none d-sm-block">
                    <div class="image-container mt-3 wow zoomIn animated">
                        <img src="assets/images/project/project-3.jpg" style="width: 100%;">
                        <div class="overlay">
                            <div class="image-text">
                                Lorem ipsum dolor sub amet
                            </div>
                        </div>
                    </div>
                    <div class="image-container mt-3 wow zoomIn animated">
                        <img src="assets/images/project/project-15.jpg" style="width: 100%;">
                        <div class="overlay">
                            <div class="image-text">
                                Lorem ipsum dolor sub amet
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column d-none d-sm-block">
                    <div class="image-container mt-3 wow zoomIn animated">
                        <img src="assets/images/project/project-4.jpg" style="width: 100%;">
                        <div class="overlay">
                            <div class="image-text">
                                Lorem ipsum dolor sub amet
                            </div>
                        </div>
                    </div>
                    <div class="image-container mt-3 wow zoomIn animated">
                        <img src="assets/images/project/project-8.jpg" style="width: 100%;">
                        <div class="overlay">
                            <div class="image-text">
                                Lorem ipsum dolor sub amet
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center pt-5 wow zoomIn animated">
            <a class="btn btn-rounded btn-primary text-center" role="button"
                href="{{ route('project') }}"> Lihat Proyek lainnya </button>
            </a>
        </div>
    </div>
</section>