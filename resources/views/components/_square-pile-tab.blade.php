<section id="square-pile-tab" class="bg-white-900 pt10 pb20">
    <div class="container no-padding">
        <div class="w3-row wow fadeInUp animated">
            <a href="javascript:void(0)" onclick="openSquare(event, 'technical');">
                <div class="w3-col m15 s3 tablink w3-bottombar w3-hover-light-grey w3-padding w3-border-wahana-blue">Technical Specification</div>
            </a>
            <a href="javascript:void(0)" onclick="openSquare(event, 'square20');">
                <div class="w3-col m15 s3 tablink w3-bottombar w3-hover-light-grey w3-padding">
                    20x20cm<br>Square Pile
                </div>
            </a>
            <a href="javascript:void(0)" onclick="openSquare(event, 'square25');">
                <div class="w3-col m15 s3 tablink w3-bottombar w3-hover-light-grey w3-padding">
                    25x25cm<br>Square Pile
                </div>
             </a>
            <a href="javascript:void(0)" onclick="openSquare(event, 'square30');">
                <div class="w3-col m15 s3 tablink w3-bottombar w3-hover-light-grey w3-padding">
                    30x30cm<br>Square Pile
                </div>
            </a>
            <a href="javascript:void(0)" onclick="openSquare(event, 'square35');">
                <div class="w3-col m15 s3 tablink w3-bottombar w3-hover-light-grey w3-padding">
                    35x35cm<br>Square Pile
                </div>
            </a>
            <a href="javascript:void(0)" onclick="openSquare(event, 'square40');">
                <div class="w3-col m15 s3 tablink w3-bottombar w3-hover-light-grey w3-padding">
                    40x40cm<br>Square Pile
                </div>
            </a>
            <a href="javascript:void(0)" onclick="openSquare(event, 'square45');">
                <div class="w3-col m15 s3 tablink w3-bottombar w3-hover-light-grey w3-padding">
                    45x45cm<br>Square Pile
                </div>
            </a>
        </div>
        <div id="technical" class="w3-container square pt-4 pb-5 wow fadeInUp animated" style="display:block">
            <h2>Technical Specification</h2>
            <p>Pre-stressed Square Concrete Pile</p>
            <div class="w3-responsive">
                <table class="w3-table-all w3-hoverable w3-centered">
                    <tr class="w3-hover-wahana">
                        <th>Dimensi Tiang<br>(cm)</th>
                        <th>Penulangan</th>
                        <th>Luas Penampang<br>(cm<sup>2</sup>)</th>
                        <th>Cracking<br>(Ton-meter)</th>
                        <th>Axial Load Allowable<br>(Ton)</th>
                        <th>Berat Per Meter<br>(kg/m)</th>
                    </tr>
                    <tr class="w3-hover-wahana">
                        <td>20x20</td>
                        <td>4 pc wire 7mm</td>
                        <td>400</td>
                        <td>0,98</td>
                        <td>50</td>
                        <td>96</td>
                    </tr>
                    <tr class="w3-hover-wahana">
                        <td>25x25</td>
                        <td>4 pc wire 7mm</td>
                        <td>625</td>
                        <td>1,60</td>
                        <td>80.8</td>
                        <td>150</td>
                    </tr>
                    <tr class="w3-hover-wahana">
                        <td>30x30</td>
                        <td>4 strand 3/8"</td>
                        <td>900</td>
                        <td>3,12</td>
                        <td>114</td>
                        <td>216</td>
                    </tr>
                    <tr class="w3-hover-wahana">
                        <td>35x35</td>
                        <td>4 strand 3/8"</td>
                        <td>1225</td>
                        <td>4,37</td>
                        <td>158.5</td>
                        <td>294</td>
                    </tr>
                    <tr class="w3-hover-wahana">
                        <td>40x40</td>
                        <td>4 strand 1/2"</td>
                        <td>1600</td>
                        <td>7,26</td>
                        <td>203.3</td>
                        <td>384</td>
                    </tr>
                    <tr class="w3-hover-wahana">
                        <td>45x45</td>
                        <td>5 strand 1/2"</td>
                        <td>2025</td>
                        <td>10,28</td>
                        <td>257.5</td>
                        <td>486</td>
                    </tr>
                </table>
            </div>
            <p class="text-bold text-20 pt-4">SPESIFIKASI BAHAN</p>
            <ul class="pt-1 pl15">
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Beton
                        </div>
                        <div class="col-7 col-md-4">
                            : K-500
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Besi Beton
                        </div>
                        <div class="col-7 col-md-4">
                            : BJTP 24, fy = 2.400 kg/cm<sup>2</sup>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Baja Prategang
                        </div>
                        <div class="col-7 col-md-4">
                            : PC WIRE JIS G 3536<br>
                            &nbsp;&nbsp;STRAND ASTM Grade 270
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div id="square20" class="w3-container square pt-4 pb-5 wow fadeInUp animated" style="display:none">
            <h2>20x20cm Square Pile</h2>
            <p>Pre-stressed 20x20cm dimension Square Concrete Pile</p>
            <img class="image-full" src="assets/images/product/square-pile/20.png" draggable="false">
            <p class="text-bold text-20 pt-4">SPESIFIKASI BAHAN</p>
            <ul class="pt-1 pl15">
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Beton
                        </div>
                        <div class="col-7 col-md-4">
                            : K-500 (fc' = 40 Mpa)
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Besi Beton
                        </div>
                        <div class="col-7 col-md-4">
                            : BJTP 24, fy = 2.400 kg/cm<sup>2</sup>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Baja Prategang
                        </div>
                        <div class="col-7 col-md-4">
                            : PC WIRE JIS G 3536<br>
                            &nbsp;&nbsp;SNI 1155 : 2011 KBjP - R
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div id="square25" class="w3-container square pt-4 pb-5 wow fadeInUp animated" style="display:none">
            <h2>25x25cm Square Pile</h2>
            <p>Pre-stressed 25x25cm dimension Square Concrete Pile</p>
            <img class="image-full" src="assets/images/product/square-pile/25.png" draggable="false">
            <p class="text-bold text-20 pt-4">SPESIFIKASI BAHAN</p>
            <ul class="pt-1 pl15">
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Beton
                        </div>
                        <div class="col-7 col-md-4">
                            : K-500 (fc' = 40 Mpa)
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Besi Beton
                        </div>
                        <div class="col-7 col-md-4">
                            : BJTP 24, fy = 2.400 kg/cm<sup>2</sup>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Baja Prategang
                        </div>
                        <div class="col-7 col-md-4">
                            : PC WIRE JIS G 3536<br>
                            &nbsp;&nbsp;SNI 1155 : 2011 KBjP - R
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div id="square30" class="w3-container square pt-4 pb-5 wow fadeInUp animated" style="display:none">
            <h2>30x30cm Square Pile</h2>
            <p>Pre-stressed 30x30cm dimension Square Concrete Pile</p>
            <img class="image-full" src="assets/images/product/square-pile/30.png" draggable="false">
            <p class="text-bold text-20 pt-4">SPESIFIKASI BAHAN</p>
            <ul class="pt-1 pl15">
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Beton
                        </div>
                        <div class="col-7 col-md-4">
                            : K-500 (fc' = 40 Mpa)
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Besi Beton
                        </div>
                        <div class="col-7 col-md-4">
                            : BJTP 24, fy = 2.400 kg/cm<sup>2</sup>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Baja Prategang
                        </div>
                        <div class="col-7 col-md-4">
                            : Strand ASTM GRADE 270
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div id="square35" class="w3-container square pt-4 pb-5 wow fadeInUp animated" style="display:none">
            <h2>35x35cm Square Pile</h2>
            <p>Pre-stressed 35x35cm dimension Square Concrete Pile</p>
            <img class="image-full" src="assets/images/product/square-pile/35.png" draggable="false">
            <p class="text-bold text-20 pt-4">SPESIFIKASI BAHAN</p>
            <ul class="pt-1 pl15">
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Beton
                        </div>
                        <div class="col-7 col-md-4">
                            : K-500 (fc' = 40 Mpa)
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Besi Beton
                        </div>
                        <div class="col-7 col-md-4">
                            : BJTP 24, fy = 2.400 kg/cm<sup>2</sup>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Baja Prategang
                        </div>
                        <div class="col-7 col-md-4">
                            : Strand ASTM GRADE 270
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div id="square40" class="w3-container square pt-4 pb-5 wow fadeInUp animated" style="display:none">
            <h2>40x40cm Square Pile</h2>
            <p>Pre-stressed 40x40cm dimension Square Concrete Pile</p>
            <img class="image-full" src="assets/images/product/square-pile/40.png" draggable="false">
            <p class="text-bold text-20 pt-4">SPESIFIKASI BAHAN</p>
            <ul class="pt-1 pl15">
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Beton
                        </div>
                        <div class="col-7 col-md-4">
                            : K-500 (fc' = 40 Mpa)
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Besi Beton (T.Spiral)
                        </div>
                        <div class="col-7 col-md-4">
                            : BJTP 24, fy = 2.400 kg/cm<sup>2</sup>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Baja Prategang (T.Utama)
                        </div>
                        <div class="col-7 col-md-4">
                            : Strand ASTM GRADE 270
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div id="square45" class="w3-container square pt-4 pb-5 wow fadeInUp animated" style="display:none">
            <h2>45x45cm Square Pile</h2>
            <p>Pre-stressed 45x45cm dimension Square Concrete Pile</p>
            <img class="image-full" src="assets/images/product/square-pile/45.png" draggable="false">
            <p class="text-bold text-20 pt-4">SPESIFIKASI BAHAN</p>
            <ul class="pt-1 pl15">
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Beton
                        </div>
                        <div class="col-7 col-md-4">
                            : K-500 (fc' = 40 Mpa)
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Besi Beton (T.Spiral)
                        </div>
                        <div class="col-7 col-md-4">
                            : BJTP 24, fy = 2.400 kg/cm<sup>2</sup>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Baja Prategang (T.Utama)
                        </div>
                        <div class="col-7 col-md-4">
                            : Strand ASTM GRADE 270
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section>