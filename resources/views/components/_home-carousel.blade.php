<div id="home-carousel" class="owl-main-slide owl-carousel carousel-dark fullscreen mb0">
    <div class="item bg-parallax fullscreen parallax-overlay"
        style='background-image: url("assets/images/home-slider/slider-1.jpg")'>
        <div class="d-flex align-items-center" style="background-color: white; height: 12.5%; top:87.5%">
            <div class="d-block d-sm-none d-md-none">
                <div class="row">
                    <div class="text-block-sm">
                        <div class="banner">
                            <p class="title">BUILDING INDONESIA</p>
                            <p class="subtitle">FOR MORE THAN 30 YEARS</p> 
                        </div>
                        <p class="tagline-sm text-center"> Solusi Pancang Tanpa Getaran </p>
                    </div>
                    <p class="tagline text-black text-medium text-center font-weight-600 fadeInDown delay-1 mt-3 ml-5">
                        Solusi Pancang Tanpa Getaran
                    </p>
                </div>
            </div>
            <div class="container d-none d-sm-block d-md-block">
                <div class="row">
                    <div class="col-md-12 ml-auto mr-auto text-center animated">
                        <div class="text-block mt-5 mb-5 pt-3 pb-3 wow zoomInRoght animated">
                            <p></p>
                            <p class="shadow-text">BUILDING INDONESIA</p>
                            <p class="shadow-small">FOR MORE THAN 30 YEARS</p>
                        </div>
                        <h3 class="tagline text-black font-weight-600 fadeInDown delay-1 bottom-right">
                            Solusi Pancang Tanpa Getaran
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item bg-parallax fullscreen parallax-overlay"
        style='background-image: url("assets/images/home-slider/slider-2.jpg")'>
        <div class="d-flex align-items-center" style="background-color: white; height: 12.5%; top:87.5%">
            <div class="d-block d-sm-none d-md-none">
                <div class="row">
                    <div class="text-block-sm">
                        <div class="banner">
                            <p class="title">BUILDING INDONESIA</p>
                            <p class="subtitle">FOR MORE THAN 30 YEARS</p> 
                        </div>
                        <p class="tagline-sm"> Solusi Pancang Tanpa Getaran </p>
                    </div>
                    <p class="tagline text-black text-medium text-center font-weight-600 fadeInDown delay-1 mt-3 ml-5">
                        Solusi Pancang Tanpa Getaran
                    </p>
                </div>
            </div>
            <div class="container d-none d-sm-block d-md-block">
                <div class="row">
                    <div class="col-md-12 ml-auto mr-auto text-center animated">
                        <div class="text-block mt-5 mb-5 pt-3 pb-3">
                            <p></p>
                            <p class="shadow-text">BUILDING INDONESIA</p>
                            <p class="shadow-small">FOR MORE THAN 30 YEARS</p>
                        </div>
                        <h3 class="tagline text-black font-weight-600 fadeInDown delay-1 bottom-right">
                            Solusi Pancang Tanpa Getaran
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item bg-parallax fullscreen parallax-overlay"
        style='background-image: url("assets/images/home-slider/slider-3.jpg")'>
        <div class="d-flex align-items-center" style="background-color: white; height: 12.5%; top:87.5%">
            <div class="d-block d-sm-none d-md-none">
                <div class="row">
                    <div class="text-block-sm">
                        <div class="banner">
                            <p class="title">BUILDING INDONESIA</p>
                            <p class="subtitle">FOR MORE THAN 30 YEARS</p> 
                        </div>
                        <p class="tagline-sm"> Solusi Pancang Tanpa Getaran </p>
                    </div>
                    <p class="tagline text-black text-medium text-center font-weight-600 fadeInDown delay-1 mt-3 ml-5">
                        Solusi Pancang Tanpa Getaran
                    </p>
                </div>
            </div>
            <div class="container d-none d-sm-block d-md-block">
                <div class="row">
                    <div class="col-md-12 ml-auto mr-auto text-center animated">
                        <div class="text-block mt-5 mb-5 pt-3 pb-3">
                            <p></p>
                            <p class="shadow-text">BUILDING INDONESIA</p>
                            <p class="shadow-small">FOR MORE THAN 30 YEARS</p>
                        </div>
                        <h3 class="tagline text-black font-weight-600 fadeInDown delay-1 bottom-right">
                            Solusi Pancang Tanpa Getaran
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item bg-parallax fullscreen parallax-overlay"
        style='background-image: url("assets/images/home-slider/slider-4.jpg")'>
        <div class="d-flex align-items-center" style="background-color: white; height: 12.5%; top:87.5%">
            <div class="d-block d-sm-none d-md-none">
                <div class="row">
                    <div class="text-block-sm">
                        <div class="banner">
                            <p class="title">BUILDING INDONESIA</p>
                            <p class="subtitle">FOR MORE THAN 30 YEARS</p> 
                        </div>
                        <p class="tagline-sm"> Solusi Pancang Tanpa Getaran </p>
                    </div>
                    <p class="tagline text-black text-medium text-center font-weight-600 fadeInDown delay-1 mt-3 ml-5">
                        Solusi Pancang Tanpa Getaran
                    </p>
                </div>
            </div>
            <div class="container d-none d-sm-block d-md-block">
                <div class="row">
                    <div class="col-md-12 ml-auto mr-auto text-center animated">
                        <div class="text-block mt-5 mb-5 pt-3 pb-3">
                            <p></p>
                            <p class="shadow-text">BUILDING INDONESIA</p>
                            <p class="shadow-small">FOR MORE THAN 30 YEARS</p>
                        </div>
                        <h3 class="tagline text-black font-weight-600 fadeInDown delay-1 bottom-right">
                            Solusi Pancang Tanpa Getaran
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item bg-parallax fullscreen parallax-overlay"
        style='background-image: url("assets/images/home-slider/slider-5.jpg")'>
        <div class="d-flex align-items-center" style="background-color: white; height: 12.5%; top:87.5%">
            <div class="d-block d-sm-none d-md-none">
                <div class="row">
                    <div class="text-block-sm">
                        <div class="banner">
                            <p class="title">BUILDING INDONESIA</p>
                            <p class="subtitle">FOR MORE THAN 30 YEARS</p> 
                        </div>
                        <p class="tagline-sm"> Solusi Pancang Tanpa Getaran </p>
                    </div>
                    <p class="tagline text-black text-medium text-center font-weight-600 fadeInDown delay-1 mt-3 ml-5">
                        Solusi Pancang Tanpa Getaran
                    </p>
                </div>
            </div>
            <div class="container d-none d-sm-block d-md-block">
                <div class="row">
                    <div class="col-md-12 ml-auto mr-auto text-center animated">
                        <div class="text-block mt-5 mb-5 pt-3 pb-3">
                            <p></p>
                            <p class="shadow-text">BUILDING INDONESIA</p>
                            <p class="shadow-small">FOR MORE THAN 30 YEARS</p>
                        </div>
                        <h3 class="tagline text-black font-weight-600 fadeInDown delay-1 bottom-right">
                            Solusi Pancang Tanpa Getaran
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item bg-parallax fullscreen parallax-overlay"
        style='background-image: url("assets/images/home-slider/slider-7.jpg")'>
        <div class="d-flex align-items-center" style="background-color: white; height: 12.5%; top:87.5%">
            <div class="d-block d-sm-none d-md-none">
                <div class="row">
                    <div class="text-block-sm">
                        <div class="banner">
                            <p class="title">BUILDING INDONESIA</p>
                            <p class="subtitle">FOR MORE THAN 30 YEARS</p> 
                        </div>
                        <p class="tagline-sm"> Solusi Pancang Tanpa Getaran </p>
                    </div>
                    <p class="tagline text-black text-medium text-center font-weight-600 fadeInDown delay-1 mt-3 ml-5">
                        Solusi Pancang Tanpa Getaran
                    </p>
                </div>
            </div>
            <div class="container d-none d-sm-block d-md-block">
                <div class="row">
                    <div class="col-md-12 ml-auto mr-auto text-center animated">
                        <div class="text-block mt-5 mb-5 pt-3 pb-3">
                            <p></p>
                            <p class="shadow-text">BUILDING INDONESIA</p>
                            <p class="shadow-small">FOR MORE THAN 30 YEARS</p>
                        </div>
                        <h3 class="tagline text-black font-weight-600 fadeInDown delay-1 bottom-right">
                            Solusi Pancang Tanpa Getaran
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item bg-parallax fullscreen parallax-overlay"
        style='background-image: url("assets/images/home-slider/slider-8.jpg")'>
        <div class="d-flex align-items-center" style="background-color: white; height: 12.5%; top:87.5%">
            <div class="d-block d-sm-none d-md-none">
                <div class="row">
                    <div class="text-block-sm">
                        <div class="banner">
                            <p class="title">BUILDING INDONESIA</p>
                            <p class="subtitle">FOR MORE THAN 30 YEARS</p> 
                        </div>
                        <p class="tagline-sm"> Solusi Pancang Tanpa Getaran </p>
                    </div>
                    <p class="tagline text-black text-medium text-center font-weight-600 fadeInDown delay-1 mt-3 ml-5">
                        Solusi Pancang Tanpa Getaran
                    </p>
                </div>
            </div>
            <div class="container d-none d-sm-block d-md-block">
                <div class="row">
                    <div class="col-md-12 ml-auto mr-auto text-center animated">
                        <div class="text-block mt-5 mb-5 pt-3 pb-3">
                            <p></p>
                            <p class="shadow-text">BUILDING INDONESIA</p>
                            <p class="shadow-small">FOR MORE THAN 30 YEARS</p>
                        </div>
                        <h3 class="tagline text-black font-weight-600 fadeInDown delay-1 bottom-right">
                            Solusi Pancang Tanpa Getaran
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item bg-parallax fullscreen parallax-overlay"
        style='background-image: url("assets/images/home-slider/slider-9.jpg")'>
        <div class="d-flex align-items-center" style="background-color: white; height: 12.5%; top:87.5%">
            <div class="d-block d-sm-none d-md-none">
                <div class="row">
                    <div class="text-block-sm">
                        <div class="banner">
                            <p class="title">BUILDING INDONESIA</p>
                            <p class="subtitle">FOR MORE THAN 30 YEARS</p> 
                        </div>
                        <p class="tagline-sm"> Solusi Pancang Tanpa Getaran </p>
                    </div>
                    <p class="tagline text-black text-medium text-center font-weight-600 fadeInDown delay-1 mt-3 ml-5">
                        Solusi Pancang Tanpa Getaran
                    </p>
                </div>
            </div>
            <div class="container d-none d-sm-block d-md-block">
                <div class="row">
                    <div class="col-md-12 ml-auto mr-auto text-center animated">
                        <div class="text-block mt-5 mb-5 pt-3 pb-3">
                            <p></p>
                            <p class="shadow-text">BUILDING INDONESIA</p>
                            <p class="shadow-small">FOR MORE THAN 30 YEARS</p>
                        </div>
                        <h3 class="tagline text-black font-weight-600 fadeInDown delay-1 bottom-right">
                            Solusi Pancang Tanpa Getaran
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item bg-parallax fullscreen parallax-overlay"
        style='background-image: url("assets/images/home-slider/slider-10.jpg")'>
        <div class="d-flex align-items-center" style="background-color: white; height: 12.5%; top:87.5%">
            <div class="d-block d-sm-none d-md-none">
                <div class="row">
                    <div class="text-block-sm">
                        <div class="banner">
                            <p class="title">BUILDING INDONESIA</p>
                            <p class="subtitle">FOR MORE THAN 30 YEARS</p> 
                        </div>
                        <p class="tagline-sm"> Solusi Pancang Tanpa Getaran </p>
                    </div>
                    <p class="tagline text-black text-medium text-center font-weight-600 fadeInDown delay-1 mt-3 ml-5">
                        Solusi Pancang Tanpa Getaran
                    </p>
                </div>
            </div>
            <div class="container d-none d-sm-block d-md-block">
                <div class="row">
                    <div class="col-md-12 ml-auto mr-auto text-center animated">
                        <div class="text-block mt-5 mb-5 pt-3 pb-3">
                            <p></p>
                            <p class="shadow-text">BUILDING INDONESIA</p>
                            <p class="shadow-small">FOR MORE THAN 30 YEARS</p>
                        </div>
                        <h3 class="tagline text-black font-weight-600 fadeInDown delay-1 bottom-right">
                            Solusi Pancang Tanpa Getaran
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item bg-parallax fullscreen parallax-overlay"
        style='background-image: url("assets/images/home-slider/slider-11.jpg")'>
        <div class="d-flex align-items-center" style="background-color: white; height: 12.5%; top:87.5%">
            <div class="d-block d-sm-none d-md-none">
                <div class="row">
                    <div class="text-block-sm">
                        <div class="banner">
                            <p class="title">BUILDING INDONESIA</p>
                            <p class="subtitle">FOR MORE THAN 30 YEARS</p> 
                        </div>
                        <p class="tagline-sm"> Solusi Pancang Tanpa Getaran </p>
                    </div>
                    <p class="tagline text-black text-medium text-center font-weight-600 fadeInDown delay-1 mt-3 ml-5">
                        Solusi Pancang Tanpa Getaran
                    </p>
                </div>
            </div>
            <div class="container d-none d-sm-block d-md-block">
                <div class="row">
                    <div class="col-md-12 ml-auto mr-auto text-center animated">
                        <div class="text-block mt-5 mb-5 pt-3 pb-3">
                            <p></p>
                            <p class="shadow-text">BUILDING INDONESIA</p>
                            <p class="shadow-small">FOR MORE THAN 30 YEARS</p>
                        </div>
                        <h3 class="tagline text-black font-weight-600 fadeInDown delay-1 bottom-right">
                            Solusi Pancang Tanpa Getaran
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>