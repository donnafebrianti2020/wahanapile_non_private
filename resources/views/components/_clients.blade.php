<section id="clients" class="pt40 pb40">
    <div class="container">
        <div class="title-heading1 mb40 wow fadeInUp animated">
            <h3>Klien Kami</h3>
        </div>
        <div class="carousel-client owl-carousel owl-theme">
            <div class="item text-center">
                <div class="carousel-client-img-wrapper pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/cargill.png') }}" alt="cargill">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/pakuwon-permai.png') }}" alt="pakuwon-permai">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3 mb-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/assa-land.png') }}" alt="assa-land">
                </div>
            </div>
            <div class="item text-center">
                <div class="carousel-client-img-wrapper pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/pertamina.png') }}" alt="pertamina">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/pullman.png') }}" alt="pullman">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3 mb-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/shell.png') }}" alt="shell">
                </div>
            </div>
            <div class="item text-center">
                <div class="carousel-client-img-wrapper pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/smelting.png') }}" alt="smelting">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/waskita-karya.png') }}" alt="waskita-karya">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3 mb-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/wika.png') }}" alt="wika">
                </div>
            </div>
            <div class="item text-center">
                <div class="carousel-client-img-wrapper pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/daya-patra-ngasem-raya.png') }}" alt="daya-patra-ngasem-raya">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/amarta-karya.png') }}" alt="amarta-karya">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3 mb-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/aneka-jasa-ghradika.png') }}" alt="aneka-jasa-ghradika">
                </div>
            </div>
            <div class="item text-center">
                <div class="carousel-client-img-wrapper pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/assa-land.png') }}" alt="assa-land">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/bangun-karya-perkasa-jaya.gif') }}" alt="bangun-karya-perkasa-jaya">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3 mb-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/brantas-abipraya.png') }}" alt="brantas-abipraya">
                </div>
            </div>
            <div class="item text-center">
                <div class="carousel-client-img-wrapper pr-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/bukaka-teknik-utama.jpg') }}" alt="bukaka-teknik-utama">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/adhi-karya.png') }}" alt="adhi-karya">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3 mb-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/guna-cipta.png') }}" alt="guna-cipta">
                </div>
            </div>
            <div class="item text-center">
                <div class="carousel-client-img-wrapper pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/himindo-citra-mandiri.png') }}" alt="himindo-citra-mandiri">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/hutama-karya.png') }}" alt="hutama-karya">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3 mb-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/jatim-bromo-steel.png') }}" alt="jatim-bromo-steel">
                </div>
            </div>
            <div class="item text-center">
                <div class="carousel-client-img-wrapper pr-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/limajabat-jaya.jpg') }}" alt="limajabat-jaya">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/mall-bali-galeria.png') }}" alt="mall-bali-galeria">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3 mb-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/mandiri-duta-contractor.png') }}" alt="mandiri-duta-contractor">
                </div>
            </div>
            <div class="item text-center">
                <div class="carousel-client-img-wrapper pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/miranthi-adhi-persada.png') }}" alt="miranthi-adhi-persada">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/mitra-gema-mandiri.png') }}" alt="mitra-gema-mandiri">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3 mb-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/mukti-adhi-sejahtera.png') }}" alt="mukti-adhi-sejahtera">
                </div>
            </div>
            <div class="item text-center">
                <div class="carousel-client-img-wrapper pr-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/multikon.png') }}" alt="multikon">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/multindo.png') }}" alt="multindo">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3 mb-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/nindya-karya-logo.png') }}" alt="nindya-karya-logo">
                </div>
            </div>
            <div class="item text-center">
                <div class="carousel-client-img-wrapper pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/nusa-raya-cipta.png') }}" alt="nusa-raya-cipta">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/pp-precast.png') }}" alt="pp-precast">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/pakuwon-jati.png') }}" alt="pakuwon-jati">
                </div>
            </div>
            <div class="item text-center">
                <div class="carousel-client-img-wrapper pr-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/pim-pharmaceutical.png') }}" alt="pim-pharmaceutical">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3 mb-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/pp-properti.png') }}" alt="pp-properti">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3 mb-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/universitas-muhammadiyah-surabaya.png') }}" alt="universitas-muhammadiyah-surabaya">
                </div>
            </div>
            <div class="item text-center">  
                <div class="carousel-client-img-wrapper pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/pp-taisei.png') }}" alt="pp-taisei">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/pulau-intan.png') }}" alt="pulau-intan">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3 mb-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/putra-mahakarya-sentosa.png') }}" alt="putra-mahakarya-sentosa">
                </div>
            </div>
            <div class="item text-center">
                <div class="carousel-client-img-wrapper pr-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/wynncor bali.png') }}" alt="wynncor bali">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/sarana-karya-utama.png') }}" alt="sarana-karya-utama">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3 mb-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/sasmito.png') }}" alt="sasmito">
                </div>
            </div>
            <div class="item text-center">
                <div class="carousel-client-img-wrapper pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/sinar-intiberkah-sejahtera.png') }}" alt="sinar-intiberkah-sejahtera">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/sriwijaya-abadi.png') }}" alt="sriwijaya-abadi">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3 mb-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/tata-bumi-raya.png') }}" alt="tata-bumi-raya">
                </div>
            </div>
            <div class="item text-center">
                <div class="carousel-client-img-wrapper pr-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/tatamulia-nusantara-indah.png') }}" alt="tatamulia-nusantara-indah">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3"> <img class="carousel-client-img"
                        src="{{ asset('assets/images/client/tiara-abadi-nirmala.png') }}" alt="tiara-abadi-nirmala">
                </div>
                <div class="carousel-client-img-wrapper mt-5 pr-3 mb-3"><img class="carousel-client-img"
                        src="{{ asset('assets/images/client/total-bangun-persada.png') }}" alt="total-bangun-persada">
                </div>
            </div>
        </div>
    </div>
</section>