<section id="pre-footer" class="cta pt20 pb40" style="background-color:#f5f5f5">
    <div class="container d-block d-lg-none">
        <div class="row justify-content-center pt-2 pb-3">
            <img class="logo logo-dark" src="assets/images/logo/wahana/wahana3.png" style="height: 75px;">
        </div>
        <div class="row justify-content-center pt-2">
            <h5 class="footer-title text-18"> 
                PT WAHANA CIPTA CONCRETINDO
            </h5>
        </div>
        <div class="row justify-content-center">
            <h6 class="text-blue text-small">
                PRECAST - PRESTRESSED CONCRETE PRODUCTS
            </h6>
        </div>
        <div class="row justify-content-center pl-4 pr-3">
            <h6 class="text-12 text-center">
                Produksi Tiang Pancang 20x20 s/d 45x45, Sheet Pile, Pagar Beton
                dan Produk Beton sesuai pesanan dan Jasa Pancang Diesel Hammer,
                Injection, Hydraulic Static & Jasa Erection Precast Product
            </h6>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <div class="row justify-content-center pt-2">
                    <h5 class="text-blue text-bold text-medium"> 
                        OUR PLANTS
                    </h5>
                </div>
                <div class="row justify-content-center">
                    <h5 class="text-bold text-small"> 
                        Bogor
                    </h5>
                </div>
                <div class="row justify-content-center">
                    <h6 class="text-12 half-line">
                        Jl Raya Trans Yogi Km. 54,
                    </h6>
                </div>
                <div class="row justify-content-center">
                    <h6 class="text-12 half-line">
                        Ds. Tegal Panjang, Kec. Cariu Bogor
                    </h6>
                </div>
                <div class="row justify-content-center">
                    <h5 class="text-bold text-small"> 
                        Gresik
                    </h5>
                </div>
                <div class="row justify-content-center">
                    <h6 class="text-12 half-line">
                        Jl Gubernur Suryo 78 A, Romo Gresik
                    </h6>
                </div>
                <div class="row justify-content-center">
                    <h5 class="text-bold text-small"> 
                        Surabaya
                    </h5>
                </div>
                <div class="row justify-content-center">
                    <h6 class="text-12 half-line">
                        Jl Dupak Rukun 106 Surabaya
                    </h6>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="row justify-content-center pt-2">
                    <h5 class="text-blue text-bold text-medium"> 
                        CONTACT US
                    </h5>
                </div>
                <div class="row justify-content-center">
                    <h5 class="text-bold text-small"> 
                        Office
                    </h5>
                </div>
                <div class="row justify-content-center">
                    <h6 class="text-12 half-line">
                        Ruko Rungkut Megah Raya Blok L-17
                    </h6>
                </div>
                <div class="row justify-content-center">
                    <h6 class="text-12 half-line">
                        Jl Raya Rungkut Surabaya
                    </h6>
                </div>
                <div class="row justify-content-center">
                    <h5 class="text-bold text-small"> 
                        Phone
                    </h5>
                </div>
                <div class="row justify-content-center">
                    <h6 class="text-12 half-line">
                        (031) 879 1555, 879 2655
                    </h6>
                </div>
                <div class="row justify-content-center">
                    <h5 class="text-bold text-small"> 
                        Fax
                    </h5>
                </div>
                <div class="row justify-content-center">
                    <h6 class="text-12 half-line">
                        (031) 879 3534
                    </h6>
                </div>
                <div class="row justify-content-center">
                    <h5 class="text-bold text-small"> 
                        Email
                    </h5>
                </div>
                <div class="row justify-content-center">
                    <h6 class="text-12 half-line">
                        info@wahanaconcrete.com
                    </h6>
                </div>
            </div>
        </div>
    </div>
    <div class="container d-none d-lg-block">
        <div class="row">
            <div class="col-md-4 text-left mt-3">
                <div class="row pb-1">
                    <img class="logo logo-dark" src="assets/images/logo/wahana/wahana3.png" style="height: 100px;">
                </div>
                <div class="row mt-4">
                    <h5 class="footer-title"> 
                        PT WAHANA CIPTA CONCRETINDO
                    </h5>
                    <h6 class="text-blue text-small">
                        PRECAST - PRESTRESSED CONCRETE PRODUCTS
                    </h6>
                </div>
                <div class="row">
                    <h6 class="text-12">
                        Produksi Tiang Pancang 20x20 s/d 45x45, Sheet Pile, Pagar Beton
                        dan Produk Beton sesuai pesanan dan Jasa Pancang Diesel Hammer,
                        Injection, Hydraulic Static & Jasa Erection Precast Product
                    </h6>
                </div>
            </div>
            <div class="col-md-3 text-left mt-3 ml-5 pl-5">
                <div class="row">
                    <h5 class="text-blue text-bold text-medium"> 
                        OUR PLANTS
                    </h5>
                </div>
                <div class="row">
                    <h5 class="text-bold text-small"> 
                        Bogor
                    </h5>
                </div>
                <div class="row">
                    <h6 class="text-12 half-line">
                        Jl Raya Trans Yogi Km. 54,
                    </h6>
                </div>
                <div class="row">
                    <h6 class="text-12 half-line">
                        Ds. Tegal Panjang, Kec. Cariu Bogor
                    </h6>
                </div>
                <div class="row mt-2">
                    <h5 class="text-bold text-small"> 
                        Gresik
                    </h5>
                </div>
                <div class="row">
                    <h6 class="text-12 half-line">
                        Jl Gubernur Suryo 78 A, Romo Gresik
                    </h6>
                </div>
                <div class="row mt-2">
                    <h5 class="text-bold text-small"> 
                        Surabaya
                    </h5>
                </div>
                <div class="row">
                    <h6 class="text-12 half-line">
                        Jl Dupak Rukun 106 Surabaya
                    </h6>
                </div>
                {{-- <div class="row justify-content-left pt-3">
                    <form method="get" action="assets/document/wahana-pile-company-profile.pdf">
                        <button type="submit" class="btn btn-primary text-white text-12">
                            Download Company Profile
                        </button>
                    </form>
                </div> --}}
            </div>
            <div class="col-md-3 text-left mt-3 ml-5 pl-5">
                <div class="row">
                    <h5 class="text-bold text-medium text-blue"> 
                        CONTACT US
                    </h5>
                </div>
                <div class="row">
                    <h5 class="text-bold text-small"> 
                        Office
                    </h5>
                </div>
                <div class="row">
                    <h6 class="text-12 half-line">
                        Ruko Rungkut Megah Raya Blok L-17
                    </h6>
                </div>
                <div class="row">
                    <h6 class="text-12 half-line">
                        Jl Raya Rungkut Surabaya
                    </h6>
                </div>
                <div class="row mt-2">
                    <h5 class="text-bold text-small"> 
                        Phone
                    </h5>
                </div>
                <div class="row">
                    <h6 class="text-12 half-line">
                        (031) 879 1555, 879 2655
                    </h6>
                </div>
                <div class="row mt-2">
                    <h5 class="text-bold text-small"> 
                        Fax
                    </h5>
                </div>
                <div class="row">
                    <h6 class="text-12 half-line">
                        (031) 879 3534
                    </h6>
                </div>
                <div class="row mt-2">
                    <h5 class="text-bold text-small"> 
                        Email
                    </h5>
                </div>
                <div class="row">
                    <h6 class="text-12 half-line">
                        info@wahanaconcrete.com
                    </h6>
                </div>
            </div>
        </div>
    </div>
</section>
