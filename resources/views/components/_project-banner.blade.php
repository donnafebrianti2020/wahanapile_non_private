<section id="project-banner" class="d-none d-lg-block bg-white-900 pt100 pb20">
    <div class="container rounded-lg" style='background-image: url("assets/images/project/project-banner-1.jpg")'>
        <div class="d-flex align-items-center pt80 pb80">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center animated">
                        <p class="shadow-title wow zoomIn animated">Proyek Kami</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="project-banner" class="d-block d-lg-none bg-white-900 pt10 pb10">
    <div class="container rounded-lg" style='background-image: url("assets/images/project/project-banner-1.jpg")'>
        <div class="d-flex align-items-center pt80 pb80">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center animated">
                        <p class="shadow-title wow zoomIn animated">Proyek Kami</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>