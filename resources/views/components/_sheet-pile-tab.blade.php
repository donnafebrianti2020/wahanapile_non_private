<section id="sheet-pile-tab" class="bg-white-900 pt10 pb20">
    <div class="container no-padding">
        <div class="w3-row wow fadeInUp animated">
            <a href="javascript:void(0)" onclick="openSheet(event, 'technical-sheet');">
                <div class="w3-quarter w3-col s3 tablink w3-bottombar w3-hover-light-grey w3-padding w3-border-wahana-blue">Technical<br>Specification Table</div>
            </a>
            <a href="javascript:void(0)" onclick="openSheet(event, 'sheet22');">
                <div class="w3-quarter w3-col s3 tablink w3-bottombar w3-hover-light-grey w3-padding">
                    22x50cm<br>Flat Sheet Pile
                </div>
            </a>
            <a href="javascript:void(0)" onclick="openSheet(event, 'sheet32');">
                <div class="w3-quarter w3-col s3 tablink w3-bottombar w3-hover-light-grey w3-padding">
                    32x50cm<br>Flat Sheet Pile
                </div>
             </a>
            <a href="javascript:void(0)" onclick="openSheet(event, 'sheet45');">
                <div class="w3-quarter w3-col s3 tablink w3-bottombar w3-hover-light-grey w3-padding">
                    45x100cm<br>Corrugated Sheet Pile
                </div>
            </a>
        </div>
        <div id="technical-sheet" class="w3-container sheet pt-4 pb-5 wow fadeInUp animated" style="display:block">
            <h2>Technical Specification</h2>
            <p>Pre-stressed Sheet Concrete Pile</p>
            <div class="w3-responsive">
                <table class="w3-table-all w3-hoverable w3-centered">
                    <tr class="w3-hover-wahana">
                        <th>Type<br>(mm)</th>
                        <th>Penulangan</th>
                        <th>Height<br>(mm)</th>
                        <th>Thickness<br>(mm)</th>
                        <th>Width<br>(mm)</th>
                        <th>Moment Cracking<br>(Ton-meter)</th>
                        <th>Weight<br>(kg/m)</th>
                    </tr>
                    <tr class="w3-hover-wahana">
                        <td>FSP 220x500</td>
                        <td>6 strand &empty; 3/8"</td>
                        <td></td>
                        <td>220</td>
                        <td>500</td>
                        <td>3,07</td>
                        <td>275</td>
                    </tr>
                    <tr class="w3-hover-wahana">
                        <td>FSP 320x500</td>
                        <td>8 strand &empty; 1/2"</td>
                        <td></td>
                        <td>320</td>
                        <td>500</td>
                        <td>8,31</td>
                        <td>400</td>
                    </tr>
                    <tr class="w3-hover-wahana">
                        <td>WSP 450x1000</td>
                        <td>8 strand &empty; 1/2"</td>
                        <td>450</td>
                        <td>120</td>
                        <td>996</td>
                        <td>26,9</td>
                        <td>454</td>
                    </tr>
                </table>
            </div>
            <img class="image-full pt-3" src="assets/images/product/sheet-pile/spesifikasi-sheet-pile.png" draggable="false">
        </div>
        <div id="sheet22" class="w3-container sheet pt-4 pb-5 wow fadeInUp animated" style="display:none">
            <h2>22x50cm Flat Sheet Pile</h2>
            <p>Pre-stressed 22x50cm dimension Flat Concrete Sheet Pile</p>
            <img class="image-full" src="assets/images/product/sheet-pile/22.png" draggable="false">
            <p class="text-bold text-20 pt-4">SPESIFIKASI BAHAN</p>
            <ul class="pt-1 pl15">
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Beton
                        </div>
                        <div class="col-7 col-md-4">
                            : K-500 (fc' = 40 Mpa)
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Baja Prategang
                        </div>
                        <div class="col-7 col-md-4">
                            : PC WIRE JIS G 3536
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Baja Non Prategang
                        </div>
                        <div class="col-7 col-md-4">
                            : BJTP 24, fy = 2.400 kg/cm<sup>2</sup>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div id="sheet32" class="w3-container sheet pt-4 pb-5 wow fadeInUp animated" style="display:none">
            <h2>32x50cm Flat Sheet Pile</h2>
            <p>Pre-stressed 32x50cm dimension Flat Concrete Sheet Pile</p>
            <img class="image-full" src="assets/images/product/sheet-pile/32.png" draggable="false">
            <p class="text-bold text-20 pt-4">SPESIFIKASI BAHAN</p>
            <ul class="pt-1 pl15">
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Beton
                        </div>
                        <div class="col-7 col-md-4">
                            : K-500 (fc' = 40 Mpa)
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Baja Prategang
                        </div>
                        <div class="col-7 col-md-4">
                            : PC WIRE JIS G 3536
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Mutu Baja Non Prategang
                        </div>
                        <div class="col-7 col-md-4">
                            : BJTP 24, fy = 2.400 kg/cm<sup>2</sup>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div id="sheet45" class="w3-container sheet pt-4 pb-5 wow fadeInUp animated" style="display:none">
            <h2>45x100cm Corrugated Sheet Pile</h2>
            <p>Pre-stressed 45x100cm dimension Corrugated Concrete Sheet Pile</p>
            <img class="image-full" src="assets/images/product/sheet-pile/45.png" draggable="false">
            <p class="text-bold text-20 pt-4">Sheet Pile W-450 A 1000</p>
            <ul class="pt-1 pl15">
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Height
                        </div>
                        <div class="col-7 col-md-4">
                            : 450 mm
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Width
                        </div>
                        <div class="col-7 col-md-4">
                            : 996 mm
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Thickness
                        </div>
                        <div class="col-7 col-md-4">
                            : 120 mm
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Cracking Moment
                        </div>
                        <div class="col-7 col-md-4">
                            : 26.9 tf.m
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-5 col-md-3">
                            Weight
                        </div>
                        <div class="col-7 col-md-4">
                            : 454 kg/m
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section>