<section id="project-table" class="cta pt20 pb20">
    <div class="container">
        <table class="table table-responsive dataTable hover wow fadeInUp animated" id="coba">
            <thead>
                <tr>
                <th scope="col">No</th>
                <th scope="col">Project Name</th>
                <th scope="col">Client</th>
                <th scope="col">City</th>
                <th scope="col">Year</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                <th scope="row">1</th>
                <td>Gudang Absolute Mojotengah Gresik</td>
                <td>PT. Adhikarya Bumi Berkat Agung</td>
                <td>Gresik</td>
                <td>2016</td>
                </tr>
                <tr>
                <th scope="row">2</th>
                <td>Gold Vitel Basuki Rachmat Sby</td>
                <td>Ir. Sandi Febrianto</td>
                <td>Surabaya</td>
                <td>2016</td>
                </tr>
                <tr>
                <th scope="row">3</th>
                <td>RS Aisyiyah Bojonegoro Kota</td>
                <td>RS Aisyiyah</td>
                <td>Bojonegoro</td>
                <td>2016</td>
                </tr>
                <tr>
                <th scope="row">4</th>
                <td>Univ. Brawijaya Kediri</td>
                <td>PT. Afdolufaiz</td>
                <td>Kediri</td>
                <td>2016</td>
                </tr>
                <tr>
                <th scope="row">5</th>
                <td>Restorasi & Perluasan Ged. Eks Pengadilan Kediri</td>
                <td>PT. Ergates Citra Mandiri</td>
                <td>Kediri</td>
                <td>2016</td>
                </tr>
                <tr>
                <th scope="row">6</th>
                <td>Swissbel Darmo Centrum</td>
                <td>PT. PP Pracetak</td>
                <td>Surabaya</td>
                <td>2015</td>
                </tr>
                <tr>
                <th scope="row">7</th>
                <td>Gedung Mushola RAMP RS Jiwa</td>
                <td>PT. Waskita Karya</td>
                <td>Surabaya</td>
                <td>2015</td>
                </tr>
                <tr>
                <th scope="row">8</th>
                <td>Hotel Batika Surabaya</td>
                <td>PT. Nusa Raya Cipta Tbk</td>
                <td>Surabaya</td>
                <td>2015</td>
                </tr>
                <tr>
                <th scope="row">9</th>
                <td>Akademi AL Surabaya</td>
                <td>PT. Brantas Abipraya</td>
                <td>Surabaya</td>
                <td>2015</td>
                </tr>
                <tr>
                <th scope="row">10</th>
                <td>Indomarco Gresik</td>
                <td>PT. Nusa Raya Cipta</td>
                <td>Gresik</td>
                <td>2014</td>
                </tr>
                <tr>
                <th scope="row">11</th>
                <td>Krone Hotel</td>
                <td>PT. Mukti Adhi Sejahtera</td>
                <td>Surabaya</td>
                <td>2014</td>
                </tr>
                <tr>
                <th scope="row">12</th>
                <td>PT. Central Protein Prima</td>
                <td>PT. Bangun Karya Perkasa Jaya</td>
                <td>Surabaya</td>
                <td>2014</td>
                </tr>
                <tr>
                <th scope="row">13</th>
                <td>Java Cocoa Plant PT. Cargill</td>
                <td>PT. Tatamulia Nusantara Indah</td>
                <td>Gresik</td>
                <td>2014</td>
                </tr>
                <tr>
                <th scope="row">14</th>
                <td>Auto 2000</td>
                <td>PT. Dinamika Furindo Nusantara</td>
                <td>Surabaya</td>
                <td>2015</td>
                </tr>
                <tr>
                <th scope="row">15</th>
                <td>Hotel Brothers Solo</td>
                <td>PT. Brothers Graha Pratama</td>
                <td>Solo</td>
                <td>2015</td>
                </tr>
            </tbody>
        </table>
    </div>
</section>