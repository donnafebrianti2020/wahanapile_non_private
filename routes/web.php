<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.landing');
})->name('landing');

Route::get('/about-us', function () {
    return view('pages.about-us');
})->name('about-us');

Route::get('/product', function () {
    return view('pages.product');
})->name('product');

Route::get('/square-pile', function () {
    return view('pages.square-pile');
})->name('square-pile');

Route::get('/sheet-pile', function () {
    return view('pages.sheet-pile');
})->name('sheet-pile');

Route::get('/service', function () {
    return view('pages.service');
})->name('service');

Route::get('/hydraulic-injection', function () {
    return view('pages.hydraulic-injection');
})->name('hydraulic-injection');

Route::get('/diesel-hammer', function () {
    return view('pages.diesel-hammer');
})->name('diesel-hammer');

Route::get('/drilling-services', function () {
    return view('pages.drilling-services');
})->name('drilling-services');

Route::get('/lateral-loading', function () {
    return view('pages.lateral-loading');
})->name('lateral-loading');

Route::get('/pile-driving', function () {
    return view('pages.pile-driving');
})->name('pile-driving');

Route::get('/static-loading', function () {
    return view('pages.static-loading');
})->name('static-loading');

Route::get('/project', function () {
    return view('pages.project');
})->name('project');

Route::get('/contact-us', function () {
    return view('pages.contact-us');
})->name('contact-us');

Route::get('/login', function () {
    return view('auth.login');
})->name('login');

Route::get('/dashboard', function () {
    return view('auth.dashboard');
})->name('dashboard');

Route::get('/project-form', function () {
    return view('auth.admin-project');
})->name('project-form');
